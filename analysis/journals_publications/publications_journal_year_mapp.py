import requests
import json
import time
import ssl
import os
import textwrap
import random
from statistics import mean, median
from datetime import date
import urllib.request
from pymongo import MongoClient


from transformers import LineByLineTextDataset

## NCBI API Key
toAddApiKey = ""

## Function to donwload any URL 
def getHTML(url, verb = False):
    ## Create an SSL session
    ssl._create_default_https_context = ssl._create_unverified_context

    ## Try to open the connection with the provided URL - return None in case of failure
    ## This open request will allow to gather content as a stream.
    try:
        ## Request explicitly to verify the SSL connection to OpenEBench using the 'verify' flag
        req = session.get(url, headers = headers, timeout = (20, 50), verify = True)
    except Exception as e:
        print(e)
        return None
    else:
        return req

## Create the session and define the request header
session = requests.Session()
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) "
    + "AppleWebKit 537.36 (KHTML, like Gecko) Chrome",
    "Accept": "text/html,application/json,application/xhtml+xml,"+
    "application/xml;q=0.9,image/webp,*/*;q=0.8",
}

def getDBtools():
    # connecting to DB
    client = MongoClient(port=27017)
    db = client.FAIRsoft
    tools_col = db.tools
    tools = []
    for tool in tools_col.find():
        tools.append(tool)
    return(tools)

def extract_publications(tools):
    publications = {} # {doi: set(publication identifiers), pmid: set(publication identifiers), pmcid = set(publication identifiers)}
    for entry in tools:
        if not "publication" in entry:
            continue
        for single_publication in entry["publication"]:
            for key in single_publication:
                publications.setdefault(key, set()).add(single_publication[key])

    # --- logging
    for record in sorted([(len(publications[k]), k) for k in publications], reverse = True):
        print(f"{record[1]}\t{record[0]}")
    # ---

    return(publications)

class mappingsGenerator:
    def __init__(self, tools, publications):
        self.tools = tools
        self.publications = publications

        ## Save the journal and mapping PubMED IDs (preferred option) for each of the entries
        self.mapping = {}
        self.mapping_year = {}
        self.pubmed_ids = {}

        ## Register IDs that have been mapped without finding any result
        self.discarded_query = set()

        ## Mapping between journal acronym and full name. From 'source' in PubMed record
        self.journal_mapping = {}

    def notFoundIDs(self, id_type):
        '''
        Returns set of ids (of type `id_type` in [doi, pmid, pmcid]) that have not been mapped yet.
        '''
        ids = sorted((self.publications[id_type] - set([self.pubmed_ids[k] for k in self.pubmed_ids])) - self.discarded_query)
        return(ids)

    def ID_to_PubMedID_NCBI(self, ids_to_query):
        '''
        Transforms first PMC IDs into PubMedIDs
        '''
        BASEURL = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&format=json&term="
        ## NCBI API artificial delay to avoid penalizations for too many concurrent requests (max 3 per second)
        delay = 1

        ## Keep the total number of PMC IDs to be queried.
        numb = len(ids_to_query)

        ## We will query NCBI PubMED in batches (size=100 PMIDs) to optimize/speed-up the whole process.
        n = 0 # to keep track of queries
        for pid in ids_to_query:
            n += 1
            ## Construct the query to NCBI
            URL = BASEURL + pid + toAddApiKey
            re = getHTML(URL)

            try:
                ## Retrieve all available tools in JSON format
                query_result = re.json()
            except Exception:
                # --- logging
                print('\r', f"{' ' * 120}", sep = '', end = '', flush = True)
                print('\r', f"WARNING\tUnexpected respose for PMC ID\t{pid}", sep = '', end = '', flush = True)
                # ---
            ## Skip current iteration in case of no results.
            if not query_result or not "esearchresult" in query_result or not "idlist" in query_result["esearchresult"]:
                self.discarded_query.add(pid)
                continue
            
            if len(query_result["esearchresult"]["idlist"]) == 1:
                self.pubmed_ids.setdefault(query_result["esearchresult"]["idlist"][0], pid)

            ## To prevent possible IP blocking for massive use of the NCBI APIs, introduce a short delay
            time.sleep(delay)
            
            # --- logging
            print('\r', f"{' ' * 120}", sep = '', end = '', flush = True)
            print('\r', f"{n/numb}", sep = '', end = '', flush = True)


    def queryEuropePMCbyDOI(self):
        ## Querying by DOIs in EuropePMC
        ##
        ## Define the API end-point, and request to establish the connection
        ##
        BASEURL = 'https://www.ebi.ac.uk/europepmc/webservices/rest/search?format=json&pageSize=1000&query=DOI:'

        n = 0
        self.found = set()
        doi_to_pubs = sorted(self.publications["doi"] - set(self.mapping.keys()))

        for doi in doi_to_pubs:
            n += 1
            if n % 250 == 0:
                print('\r', f"{' ' * 120}", sep = '', end = '', flush = True)
                print('\r', f"{len(self.found)/n}", sep = '', end = '', flush = True)
            
            ## Detect wrongly formatted DOIs, flag them and skip looking for them
            if doi.find("http") != -1:
                self.mapping.setdefault(doi, set()).add("badly-formatted")
                print('\r', f"%s" % (" " * 120), sep = '', end = '', flush = True)
                print('\r', f"WARNING\tUnexpected DOI\t'%s'" % (doi), sep = '', end = '', flush = True)
                continue
                
            if doi.find("doi") != -1:
                self.mapping.setdefault(doi, set()).add("badly-formatted")
                print('\r', f"%s" % (" " * 120), sep = '', end = '', flush = True)        
                print('\r', f"WARNING\tUnexpected DOI\t'%s'" % (doi), sep = '', end = '', flush = True)
                continue    

            ## Construct the query to EuropePMC
            URL = BASEURL + doi
            re = getHTML(URL)

            ## Retrieve all available tools in JSON format
            try:
                ## Retrieve all available tools in JSON format
                query_result = re.json()
            except:
                query_result = None
            
            ## Skip current iteration in case there is no results.
            if not query_result or not "resultList" in query_result or not "result" in query_result["resultList"]:
                continue
                
            ## We only consider unequivocally mapping results - ignore the rest
            if len(query_result["resultList"]["result"]) != 1:
                self.mapping.setdefault(doi, set()).add("unexpected-response")
                print('\r', f"%s" % (" " * 120), sep = '', end = '', flush = True)
                print('\r', f"WARNING\tUnexpected respose for DOI\t'%s'" % (doi), sep = '', end = '', flush = True)
                continue

            ## As results are unique, get the first element of the list    
            record = query_result["resultList"]["result"][0]
            
            ## Skip those cases where the Journal Name is not specified
            if not "journalTitle" in record:
                continue
                
            ## Save the Journal Name for the queried PMID
            journal_name = record["journalTitle"]
            year = record.get('pubYear', None)
            self.mapping.setdefault(doi, []).append({'journalTitle': journal_name, 'yearPub':year})
            
            ## Keep the PMID - if any - associated to the DOI
            if "pmid" in record:
                self.pubmed_ids.setdefault(record["pmid"], doi)
                
            ## Keep which DOIs have been found to avoid computing them again
            self.found.add(doi)       
            
        # --- logging
        print('\r', f"{' ' * 120}", sep = '', end = '', flush = True)
        print('\r', f"{len(self.found)/n}", sep = '', end = '', flush = True)

    def queryNCBIbyPMID(self):
        ## Querying PubMED IDs via NCBI
        
        ## Esblish the BASE URL for querying NCBI
        BASEURL  = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&retmode=json"
        BASEURL += "&retmax=100000&rettype=abstract&id="

        ## Include among the PubMED IDs to look at those detected via DOIs (EuropePMC mapping) and those
        ## converted from their equivalent PMC IDs.
        pmids = sorted(((publications["pmid"] - set(self.mapping.keys())) | set(self.pubmed_ids.keys())))
        num_entries = len(pmids)

        ## NCBI API artificial delay to avoid penalizations for too many concurrent requests (max 3 per second)
        delay = 1

        found = set()
        batch_size = 100

        ## We will query NCBI PubMED in batches to optimize/speed-up the whole process.
        ## We will use batches of 100 PMIDs.
        for start in range(0, num_entries, batch_size):
            
            ## Compute start and end for the list slice.
            stop = start + batch_size
            if stop > num_entries:
                stop = num_entries
            
            ## Join all selected PMIDs
            selected_pmids = pmids[start:stop]
            query = "+".join(map(str, selected_pmids))
            
            ## Construct the query to NCBI
            URL = BASEURL + query + toAddApiKey
            re = getHTML(URL)

            ## Retrieve all available tools in JSON format
            if re:
                query_result = re.json()
            else:
                query_result = None
            
            ## Skip current iteration in case there is no results.
            if not query_result or not "result" in query_result:
                continue
            
            ## Parse returned results
            for pmid in query_result["result"]["uids"]:
                ## Skip those cases where the Journal Name is not specified
                if not "fulljournalname" in query_result["result"][pmid]:
                    continue
                
                ## Control for unexpectedly returned results
                if not pmid in selected_pmids:
                    print('\r', f"WARNING\tUnexpected PMID\t%s" % (pmid), sep = '', end = '', flush = True)
                    self.mapping.setdefault(pmid, set()).add("unexpected-response-pmid")
                    continue
                
                ## Save the Journal Name for the queried PMID
                journal_name = query_result["result"][pmid]["fulljournalname"]
                year= query_result["result"][pmid].get('pubdate', None)
                self.mapping.setdefault(pmid, []).append({'journalTitle': journal_name, 'yearPub':year})
                
                ## Save the publication source as it can be used for the mapping between
                ## Journals' full name and Journals' acronym.
                source = query_result["result"][pmid]["source"]
                self.journal_mapping.setdefault(journal_name, source)
                
                found.add(pmid)
                
            ## To prevent possible IP blocking for massive use of the NCBI APIs, introduce a short delay
            time.sleep(delay)
            
            ## Inform about the process
            print('\r', f"{len(found)/num_entries}", sep = '', end = '', flush = True)
        
    def queryPubMedIDs(self):
        ## Querying PubMED IDs via NCBI
        # 1. Transform PMC IDs into PubMedIDs (NCBI)
        pmc_ids = self.notFoundIDs("pmcid")
        self.ID_to_PubMedID_NCBI(pmc_ids)
        # 2. Transform remaining records DOIs into PubMed IDs (NCBI)
        doi_to_pubs = self.notFoundIDs("doi")
        self.ID_to_PubMedID_NCBI(doi_to_pubs)
    
    def queryPubRecords(self):
        # Populating the mappings (id - journal name)
        # 3. Query records by DOI (EuropePMC)
        self.queryEuropePMCbyDOI()
        # 4. Qury recocords by PMID (NCBI)
        self.queryNCBIbyPMID()
        print(self.mapping)


def tools_publications_stats(all_tools, tools_publications, individual_publications):
    ## Infer some initial stats about the number of tools, tools with publications, how many
    ## unique publication has been detected and how many are associated to tools.
    ## It is possible that a given publication is associated to more than one tool entry e.g.
    ## workflows systems like Galaxy might be associated to more than one.
    n = 0
    for tool_label in tools_publications:
        n += len(tools_publications[tool_label])

    print (f"%-40s\t{len(all_tools):,}" % ("Total Tools"))
    print (f"%-40s\t{len(tools_publications):,}" % ("Tools w/Publications"))
    print (f"%-40s\t{n:,}" % ("Registered publications w/Tools"))
    print (f"%-40s\t{len(individual_publications):,}" % ("Registered publications"))

    frequency = []
    for tool_label in all_tools:
        n = 0 
        if tool_label in tools_publications:
            n = len(tools_publications[tool_label])
        frequency.append(n)

    print (f"%-40s\t{round(mean(frequency), 2):,}" % ("Average publications per tool"))
    print (f"%-40s\t{round(median(frequency), 2):,}" % ("Median publications per tool"))
    excluding_zeros = [k for k in frequency if k != 0]
    print (f"%-40s\t{round(mean(excluding_zeros), 2):,}" % ("Average publications per tool w/publications"))
    print (f"%-40s\t{round(median(excluding_zeros), 2):,}" % ("Median publications per tool w/publications"))

    counting = [(pubs, frequency.count(pubs)) for pubs in range(max(frequency)+1)]

    print ("")
    for record in counting:
        print (f"Tools with \t{record[0]:,}\tpublications\t{record[1]:,}")

    selected_tools = 0
    tools_to_select = 10
    min_publications_cut = -1
    for record in sorted(counting, reverse = True):
        if selected_tools < tools_to_select:
            selected_tools += record[1]
            min_publications_cut = record[0]

    print (f"%-60s\t{tools_to_select:,}" % ("Number of tools to Select"))        
    print (f"%-60s\t{selected_tools:,}" % ("Number of selected tools"))
    print (f"%-60s\t{min_publications_cut:,}" % ("Minimum number of publications per selected tools"))

    selected = [(len(tools_publications[tool_label]), tool_label) for tool_label in all_tools if tool_label in tools_publications and len(tools_publications[tool_label]) >= min_publications_cut]

    print (f"---")
    for record in sorted(selected, reverse = True):
        print (f"%3d\t%-30s\thttps://openebench.bsc.es/tool/%s" % (record[0], record[1], record[1]))


def build_individual_publications(tools):
    individual_publications = {}

    for tool_record in tools:
        label = tool_record["@label"]
        all_tools.add(label)

        if not "publication" in tool_record or not tool_record["publication"]:
            continue
            
        ## For any publication associate to the tool, generate an unique key formed by the sorted
        ## concatenation of id type e.g. doi, pmid, etc, and the value itself.
        ## Such key will be used to track any information associated to it at the tool level
        for publication_collection in tool_record["publication"]:
            if not publication_collection:
                continue
            key = "".join(sorted([k+publication_collection[k] for k in publication_collection]))
            tools_publications.setdefault(label, set()).add(key)
            individual_publications.setdefault(key, publication_collection)
    
    #Calculate stats
    tools_publications_stats(all_tools, tools_publications, individual_publications)
    return(all_tools, tools_publications, individual_publications)

def get_publications_to_journal(individual_publications, mappingsG):
    toExamine = {}
    publications_to_journal = {}

    for key in individual_publications:
        publication_collection = individual_publications[key]
        
        journal_name_resolution = set()
        for entry in publication_collection:
        
            journal_name = ""
            if publication_collection[entry] in mappingsG.mapping:
                if len(mappingsG.mapping[publication_collection[entry]]) == 1:
                    entry_data = list(mappingsG.mapping[publication_collection[entry]])[0]

                    if type(entry_data) == dict and 'journalTitle' in entry_data.keys():
                        if journal_name in mappingsG.journal_mapping:
                            journal_name = mappingsG.journal_mapping[journal_name]
                        else:
                            pass
                else:
                    journal_name = "conflicting"
            journal_name_resolution.add(journal_name)
            
        if len(journal_name_resolution) == 1:
            name = list(journal_name_resolution)[0]
            publications_to_journal.setdefault(key, name)
            
            if not name:
                toExamine.setdefault("empty-id-resolution", {}).setdefault(key, {})
                toExamine["empty-id-resolution"][key].setdefault("original-entry", publication_collection)
                toExamine["empty-id-resolution"][key].setdefault("name-resolution", journal_name_resolution)
            
        elif len([k for k in journal_name_resolution if k]) == 1:
            name = [k for k in journal_name_resolution if k][0]
            publications_to_journal.setdefault(key, name)
            
            toExamine.setdefault("missing-id-resolution", {}).setdefault(key, {})
            toExamine["missing-id-resolution"][key].setdefault("original-entry", publication_collection)
            toExamine["missing-id-resolution"][key].setdefault("name-resolution", journal_name_resolution)
            
        else:
            toExamine.setdefault("conflicting-id-resolution", {}).setdefault(key, {})
            toExamine["conflicting-id-resolution"][key].setdefault("original-entry", publication_collection)
            toExamine["conflicting-id-resolution"][key].setdefault("name-resolution", journal_name_resolution)
        
        ## --- issues
        print (f"%-40s\t{len(individual_publications):,}" % ("Registered publications"))
        print (f"%-40s\t{len(publications_to_journal):,}" % ("Publications' IDs with assigned Journal Name"))

        print (f"--\n\nIssues detected while mapping Journal Names across multiple IDs")
        n = 0
        for record in sorted([(len(toExamine[key]), key) for key in toExamine], reverse = True):
            print (f"> %-38s\t{record[0]:,}" % (record[1]))
            n += record[0]
        print (f"\n%-40s\t{n:,}" % ("Total Issues"))

        PATH = "./"
        filename = ("%s.detected_issues.tsv") % (date.today().strftime('%Y%m%d'))
        output_file = open(os.path.join(PATH, filename), "w")
                                                
        for detected_issue in sorted(toExamine):
            for key in sorted(toExamine[detected_issue]):
                print ((f"%-12s\t%60s\t%s") % (detected_issue, toExamine[detected_issue][key]["original-entry"], toExamine[detected_issue][key]["name-resolution"]), file = output_file)
        output_file.close()  
        ## ---
    
    return(publications_to_journal)

def frequencies_pubs_tools(tools_publications, publications_to_journal):

    freqs_only_publications = [publications_to_journal[pub] for pub in publications_to_journal]

    freqs_tools_publications = []
    for tool_label in tools_publications:
        for pub in tools_publications[tool_label]:
            if pub in publications_to_journal:
                freqs_tools_publications.append(publications_to_journal[pub])

    n, m = 0, 0
    for record in sorted([(freqs_only_publications.count(k), k) for k in set(freqs_only_publications)],                         reverse = True):

        freq = freqs_tools_publications.count(record[1])
        m += freq
        n += record[0]
        if record[0] < 100 or record[1] in ["", "unexpected-response"]:
            continue
        print ((f"%-30s\tEntries\t{record[0]:,}\tEntries mapped to tools\t{freq:,}")
                % (record[1]))    

    print (f"---")    
    print (f"%-60s\t{n:,}" % ("Publications' IDs with assigned Journal Name"))
    print (f"%-60s\t{m:,}" % ("Publications' IDs with assigned Journal Name mapped to tools"))



if __name__=='__main__':

    ## Get OEB tools
    ## retrieve all OEB tools
    tools = getDBtools()

    ## --- --- Publications Mappings --- ---
    publications = extract_publications(tools)
    # Getting pmcids
    mappingsG = mappingsGenerator(tools, publications)
    mappingsG.queryPubMedIDs()
    # Querying records
    mappingsG.queryPubRecords()


    ## --- Associating previous information (journal names) to publications in Tools --- 
    ## Register tools and publications 
    ## We bring publications at the tools level rather than at the version, deployment, authority level
    ## to reduce potential biases.
    all_tools = set()
    tools_publications = {}

    ## To optimize the cross-check across different IDs for the same publication, we keep unique entries
    ## for any detected publication
    all_tool, tools_publications, individual_publications = build_individual_publications(tools)
    publications_to_journal = get_publications_to_journal(individual_publications, mappingsG)
    frequencies_pubs_tools(tools_publications, publications_to_journal)

    '''
    with open('../data/individual_publications.json', 'w') as outputjson:
        json.dump(individual_publications, outputjson) 

    with open('../data/publications_to_journal.json', 'w') as outputjson:
        json.dump(publications_to_journal, outputjson)
    '''
    for pub in mappingsG.mapping:
        mappingsG.mapping[pub] = list(mappingsG.mapping[pub])

    with open('../data/mappings_year.json', 'w') as outputjson:
        json.dump(list([mappingsG.mapping]), outputjson)

    with open('../data/journal_mappings.json', 'w') as outputjson:
        json.dump(mappingsG.journal_mapping, outputjson) 
