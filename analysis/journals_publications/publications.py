from json import tool
import requests
import json
import time
import ssl
import re
import os
import textwrap
import random
from pymongo import MongoClient
from statistics import mean, median
from datetime import date
import urllib.request
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt

from transformers import LineByLineTextDataset

#### Calculating publications stats 

'''
# Publications data from publications_journal_year_mapp.py used in this script

## mappings_journal_year_path
 [{
     "0.1093/bioinformatics/btz540": ["unexpected-response"], 
     "10.1001/JAMANETWORKOPEN.2020.14661": [{"journalTitle": "JAMA Netw Open", "yearPub": "2020"}], 
     ...
   }]

## tools_publications
{
    "ps2-v3": ["pmid25943546"], 
    "ps2_-_v3": ["pmid25943546"],
    "aascatterplot": ["doi10.1093/bioinformatics/btx203"],
    ...
}

## Journal mappings
{
    "Science (New York, N.Y.)": "Science", 
    "Methods in molecular biology (Clifton, N.J.)": "Methods Mol Biol", 
    "Protein engineering": "Protein Eng",
    ...
}
'''    

def get_mappings_data(mappings_journal_year_path,
                      pubs_path,
                      journal_mappings_path):
    with open(mappings_journal_year_path, 'r') as infile:
        mappings = json.load(infile)
            
    with open(pubs_path, 'r') as infile:
        tools_pubs = json.load(infile)

    with open(journal_mappings_path, 'r') as infile:
        journal_mappings = json.load(infile)

    return(mappings, tools_pubs, journal_mappings)


def get_cits(citations, year_cit):
    citas = 0
    for entry in citations:
        if int(entry['year'])==year_cit:
            citas = citas+entry['count']
    return(citas)

def publication_citations(tools, year):
    pub_cits={}
    n = 0 
    ids ={}
    year = year
    seen_pubs = set()
    for entry in tools:
        n = n+1
        if 'publication' in entry.keys():
            for pub in entry['publication']:
                if type(pub)==dict:
                    ids[str(n)] = []
                    for key in ['doi','pmid','pmcid']:
                        if pub.get(key):
                            ids[str(n)].append(pub[key])
                            if 'citations' in pub.keys():
                                if pub[key] in seen_pubs:
                                    continue
                                else:
                                    pub_cits[pub[key]] = get_cits(pub['citations'], year)
                                    seen_pubs.add(pub[key])
    return(pub_cits, ids)

def inverse_ids(ids):
    inv_ids={}
    for k in ids.keys():
        for id_ in ids[k]:
            inv_ids[id_]=k
    
    return(inv_ids)

def citations_to_jour_year_mappings(mappings, pub_cits, inv_ids, ids):
    new_mapping = {}
    seen_pubs = []
    seen_ids=[]
    failed=0
    for pub in mappings[0].keys():
        if pub in seen_pubs or pub in seen_ids:
            continue
        else:
            try:
                seen_pubs.append(inv_ids[pub])
                for i in ids[inv_ids[pub]]:
                    seen_ids.append(i)
            except:
                failed += 1
                continue
            else:
                if mappings[0][pub] and mappings[0][pub][0]!='unexpected-response':
                    if mappings[0][pub][0] != 'badly-formatted':
                        mappings[0][pub][0]['cits']=pub_cits.get(pub)
                        new_mapping[pub] = mappings[0][pub][0]
    print(f"Failed {failed}")
    return(new_mapping)

def get_year(year_string):
    year_reg =r'^\d{4}$'
    full_date_reg = r'(\d{4})\s(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)+(\s\d{1,2})?'
    match_date = re.match(full_date_reg, year_string)
    match_year = re.match(year_reg, year_string)
    if match_year:
        return(int(year_string))
    elif match_date:
        year = match_date.group(1)
        return(int(year))
    else:
        return(None)


def translate_journal_name(new_mapping, journal_mappings):
    n_none=0
    n=0
    journals = set()
    for a in new_mapping.keys():
        jname=new_mapping[a]['journalTitle']
        if jname in journal_mappings.keys():
            jname = journal_mappings[jname]
        new_mapping[a]['journalTitle'] = jname   
        journals.add(jname)
        if new_mapping[a]['cits'] == None:
            n_none+=1
        else:
            n+=1
    return(new_mapping, journals)


# ### Computing impact factor
def compute_impact_factor(new_mapping, n_years, year_cits, journals):
    journal_counts = {}
    citation_counts = {}
    for jour in journals:
        journal_counts[jour]=0
    for jour in journals:
        citation_counts[jour]=0

    for k in new_mapping.keys():
        year=get_year(new_mapping[k]['yearPub'])
        if year:
            year_sub = year_cits-n_years-1
            year_up = year_cits
            if year>(year_sub) and year<(year_up):
                j = new_mapping[k]['journalTitle']
                journal_counts[j] +=1 
                c = new_mapping[k]['cits']
                if c:
                    citation_counts[j] += c
        else:
            print(new_mapping[k]['yearPub'])

    impact = {}
    for k in citation_counts.keys():
        if journal_counts[k]>0:
            impact[k] = citation_counts[k]/journal_counts[k]
        else:
            impact[k] = 0
    
    return(impact, journal_counts)


# #### Ranking journals <a class="anchor" id="ranking_journals"></a>

# ##### Ranking journals by publications count<a class="anchor" id="ranking_count_pubs"></a>
def rank_by_publications(journal_counts):
    sum = 0
    count_dict = {}
    journals_ordered = []
    for w in sorted(journal_counts, key=journal_counts.get, reverse=True):
        #print(w, journal_counts[w])
        count_dict[w]=journal_counts[w]
        journals_ordered.append(w)
        sum+=journal_counts[w]
    print(f"Total publications in journals {sum}")
    return(count_dict, journals_ordered)
        
# ##### Ranking journals by research software publications impact factor<a class="anchor" id="ranking_impact"></a>
def rank_by_software_IF(impact):
    impact_dict = {}
    for w in sorted(impact, key=impact.get, reverse=True):
        impact_dict[w]=impact[w]
    
    return(impact_dict)
    
'''
cits = []
for a in new_mapping.keys():
    if new_mapping[a]['cits'] == None:
        continue
    cits.append(new_mapping[a]['cits'])
    
    if new_mapping[a]['cits'] > 8000:
        print(a)
        print(new_mapping[a])
'''

def compute_journal_total_cits(new_mapping, year_cits, n_years, journals_top):
    journal_total_cits = {}
    for jour in journals_top:
        journal_total_cits[jour] = []

    for pub in new_mapping.keys():
        year=get_year(new_mapping[pub]['yearPub'])
        year_sub = year_cits-n_years-1
        year_up = year_cits
        if year:
            if year>(year_sub) and year<(year_up):
                journal = new_mapping[pub]['journalTitle']
                cits = new_mapping[pub]['cits']
                try:
                    journal_total_cits[journal].append(cits)
                except KeyError:
                    print(f"Journal '{journal}' not found!!!")
        else:
            print(f"Strange date {new_mapping[pub]['yearPub']}")

    return(journal_total_cits)



## publications fetcher

def getHTML(url:str)-> None:
    '''
    Makes request of the input url and returns response
    input: url
    '''
    session = requests.Session()
    headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit 537.36 (KHTML, like Gecko) Chrome"}
    try:
        re = session.get(url, headers=headers)
        return(re)

    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

## The kind of query we want:
# ("Bioinformatics"[Journal:__jid9808944] AND 2018/01/01:2018/12/31[Date - Publication]) AND (2018/1/1:2018/12/31[pdat])

def build_journal_term(j_id, j_name):
    template = '"{j_name}"[Journal:__jid{j_id}]'
    return(template.format(j_name=j_name, j_id=j_id))

def build_pubdates_term(year):
    template='{year}/01/01:{year}/12/31[pdat]'
    return(template.format(year=year))

def esearch_journal_year(journal, journal_id, year):
    first_query_template='https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={journal}&WebEnv=<webenv_string>&usehistory=y&retmode=json'
    # make first query for journal A
    url=first_query_template.format(journal=build_journal_term(journal_id, journal))
    print(url)
    first_re=getHTML(url)
    first=json.loads(first_re.text)
    # take <WebEnv>
    webenv=first['esearchresult']['webenv']
    querykey=first['esearchresult']['querykey']
    # Make intersection query based on year
    second_query_template = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={pub_dates}&query_key={querykey}&WebEnv={webenv}&usehistory=y&retmode=json'
    url=second_query_template.format(pub_dates=build_pubdates_term(year), querykey=querykey, webenv=webenv)
    intersect_re=getHTML(url)
    intersect=json.loads(intersect_re.text)
    # For each journal, find the papers we already have from tools and find them in the lists
    return(intersect)

# build a dict of journal_name => journal id from ftp://ftp.ncbi.nih.gov/pubmed/J_Medline.txt
with open('data/journals_ids.json', 'r') as ids_dict_in:
    jour_ids = json.load(ids_dict_in)

# for the ten top pubs journals, fetch publications in the selected years
def fetch_papers_years(year, years, jour_ids, journals_top):
    list_years = range(year-years, year)
    journals_counts = {}
    for journal in journals_top:
        resps = {}
        journ_count = 0
        for year in list_years:
            count =  esearch_journal_year(journal, jour_ids[journal]['NlmId'], year)['esearchresult']['count']
            journ_count += int(count)
            resps[year] = count
        resps['total']=journ_count
        journals_counts[journal] = resps

    print([y for y in list_years])
    return(journals_counts)

def years_in_period(years, year):
    list_years = [year]
    for i in range(years):
        list_years.append(1)


def pub_stats_period(new_mapping, years, year, journals):
    '''
    years: perios of time considerer for IF calculation (2 years, 3 years, ...)
    year: last year for IF calculation 
    '''
    # Compute impact factor of software publications per journal
    impact, journal_counts = compute_impact_factor(new_mapping, years, year, journals)
    #print(journal_counts)
    # Rank journals by number of software publications
    count_dict, journals_ordered = rank_by_publications(journal_counts)
    ## top journals to be analysed
    journals_top=journals_ordered[:10]
    # Compute sum of citations obtained by software-specific articles by journal
    journal_total_cits = compute_journal_total_cits(new_mapping, year, years, journals_ordered)

    # Rank by IF of software publications 
    impact_dict =  rank_by_software_IF(impact)
    
    ## fetch number of papers in each top journal per year
    journals_counts = fetch_papers_years(year, years, jour_ids, journals_top)
    print(f"TOP JOURNALS: {journals_top}")
    print(f"len ids {len(ids)}")

    # Compute IF without the tools articles
    with open('data/journals_impact_factor.json', 'r') as IFSfile:
        IFj = json.load(IFSfile)
        print(IFj)

    IFjs={}
    print(journals_top)
    for jour_name in journals_top:
        IFjs[jour_name]=IFj[jour_name][str(year)]['IF']
    print(IFjs)

    # Journal impact factor minus tools
    # Not used in the figure
    #IF(NT) = (P IF - PT IFT)/(P - PT)
    #IF(NT)=(papers*IF - Papers_tools*IF_tools)/(Papers - Papers_tools)
    IF_NT_dict={}
    IF_NT_l=[]
    for journal in journals_top:
        P=journals_counts[journal]['total']
        IF = IFjs[journal]
        PT = count_dict[journal]
        IFT= impact_dict[journal]
        
        IF_NT= (P*IF - PT*IFT)/(P - PT)
        IF_NT_dict[journal] = IF_NT
        IF_NT_l.append(IF_NT)
        
    # ---- PLOT ----
    fig = make_subplots(
        rows=2, cols=1, 
        vertical_spacing = 0.05, 
        row_heights=[300,500],
        shared_xaxes=True
    )

    #Percentage of publications 
    text_tools = [count_dict[j] for j in journals_top]
    text_journals = [journals_counts[j]['total'] for j in journals_top]
    fig.add_trace(go.Scatter(x=journals_top,
                            y=[count_dict[j]/journals_counts[j]['total'] for j in journals_top], 
                            name="Research Software publications <br> over total publications", 
                            marker_color='#205f1c',
                            mode="lines+markers+text",
                            text = [f"{count_dict[j]:,g} / {journals_counts[j]['total']:,g}" for j in journals_top],
                            textposition=['top right','bottom left','bottom left', 'bottom center','top center',
                                        'bottom center', 'top center', 'bottom center', 'top center', 'bottom center'],
                            marker_size=6,
                            line_width=1.5),
                row=1, col=1)


    y_impact=[]
    for journal in journals_top:
        y_impact.append(impact_dict[journal])

    # Impact factor of software papers 
    fig.add_trace(go.Scatter(x=journals_top, 
                            y=y_impact, 
                            name="Research Software publications", 
                            marker_color='#6ab15f',
                            marker_size=6,
                            line_width=2),
                row=2, col=1)


    # Impact factor of journals 
    fig.add_trace(go.Scatter(x=journals_top, 
                            y=[IFj[journal][str(year)]['IF'] for journal in journals_top], 
                            name="All publications", 
                            marker_color='#6ab15f',
                            marker_size=6,
                            line_width=2,
                            line = dict(dash='dash')),
                row=2, col=1)

    # Impact factor of journals without tools 
    """
    fig.add_trace(go.Scatter(x=journals, 
                            y=IF_NT_l, 
                            name="Non-software publications", 
                            marker_color='coral',
                            marker_size=6,
                            line_width=2,
                            line = dict(dash='dash')),
                row=2, col=1)
    """
    fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        y=1.02,
        xanchor="right",
        x=1
    ))
    fig.update_layout(template='plotly_white', height=500, width=800)
    fig.update_yaxes(title_text="Percentage", tickformat="%", range=[-0.07,0.51], dtick=0.1, row=1, col=1)
    fig.update_yaxes(title_text="Impact factor (five years)", range=[0,55], dtick=10, row=2, col=1)
    fig.show()
    fig.write_image("plots/publications.svg")

# connecting to DB
def connect_db_tools():
    client = MongoClient(port=27017)
    #db = client.FAIRsoft
    db = client.FAIRsoft
    tools = db.tools.find()
    return(tools)


if __name__=='__main__':

    mappings_journal_year_path = 'data/mappings_year.json'
    pubs_path = 'data/tools_publications.json'
    journal_mappings_path = 'data/journal_mappings.json'

    '''
    mappings_journal_year_path = '/home/eva/projects/FAIRsoft/publications-stats/mappings_year.json'
    pubs_path = '/home/eva/projects/FAIRsoft/publications-stats/tools_publications.json'
    journal_mappings_path = '/home/eva/projects/FAIRsoft/publications-stats/journal_mappings.json'
    '''

    mappings, tools_pubs, journal_mappings = get_mappings_data(mappings_journal_year_path, pubs_path, journal_mappings_path)
    

    print(len(mappings[0]))
    sum = 0
    for a in tools_pubs.keys():
        sum+= len(tools_pubs[a])
    print(f"len tools publications {sum}")

    year = 2021

    tools = connect_db_tools()


    # Get citations of each publication and mapping of ids
    pub_cits, ids = publication_citations(tools, year)
    print(f'len ids {len(ids.keys())}')
    #print(ids)
    inv_ids = inverse_ids(ids)
    print(f'len inverse {len(inv_ids.keys())}')
    # Add citation number to publication metadata (journal and year)
    new_mapping = citations_to_jour_year_mappings(mappings, pub_cits, inv_ids, ids)
    print(f"new mapping length {len(new_mapping)}")
    new_mapping, journals = translate_journal_name(new_mapping, journal_mappings)

    pub_stats_period(new_mapping, 5, year, journals)



    '''
    journals_counts_two = pub_stats_period(2, 2020)
    with open('data/journals_counts_two.json','w') as outjson:
        json.dump(journals_counts_two, outjson)
    '''

 

