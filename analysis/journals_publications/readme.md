# Research Software analysis 

## Publications 
Two aspects are analysed: (1) journals publishing more software-specific articles and (2) comparison of the impact factor of software and journal. 
Scripts doing the necessary calculations: 
- **publications_journal_year_mapp.py**: fetches journal and year of each publication. Generates the following data:

  - mappings_journal_year_path
    ```
    [{
        "0.1093/bioinformatics/btz540": ["unexpected-response"], 
        "10.1001/JAMANETWORKOPEN.2020.14661": [{"journalTitle": "JAMA Netw Open", "yearPub": "2020"}], 
        ...
    }]
    ```
  - tools_publications
    ```
    {
        "ps2-v3": ["pmid25943546"], 
        "ps2_-_v3": ["pmid25943546"],
        "aascatterplot": ["doi10.1093/bioinformatics/btx203"],
        ...
    }
    ```
  - Journal mappings
    ```
    {
        "Science (New York, N.Y.)": "Science", 
        "Methods in molecular biology (Clifton, N.J.)": "Methods Mol Biol", 
        "Protein engineering": "Protein Eng",
        ...
    }
    ``` 

- **publications.py**: computes the most common journals and impact factor of software publications. 
>:bulb: impact factors of journals are obtained from journal homepage 
