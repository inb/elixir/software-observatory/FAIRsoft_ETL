import json
import plotly

from pymongo import MongoClient
import plotly.io as pio
from bunch import bunchify
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt
import json

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.tools

# ### Data extraction <a class="anchor" id="data"></a>
# # #### Instances per source <a class="anchor" id="instances_per_source"></a>

Counts = {'1':0, '2':0, '3':0, '4':0, '5':0, '6':0, '7':0, '8':0, '9':0}
Counts_cummulative = {'1':0, '2':0, '3':0, '4':0, '5':0, '6':0, '7':0, '8':0, '9':0}
count_source = {}
sources_lab ={'github', 'galaxy','toolshed','bioconda', 'biotools', 'bioconductor', 'sourceforge', 'bitbucket', 'opeb_metrics'}
for lab in sources_lab:
    count_source[lab] = {}

source_match ={'bioconda':'bioconda', 'bioconda_recipes':'bioconda', 'bioconda_conda':'bioconda',
               'galaxy_metadata':'toolshed', 'toolshed':'toolshed',
               'github':'github',
               'biotools':'biotools',
               'bioconductor':'bioconductor',
               'sourceforge':'sourceforge',
               'bitbucket':'bitbucket',
               'opeb_metrics':'opeb_metrics',
               'galaxy':'galaxy'}     
    
N = 0
for tool in tools.find():
    N = N + 1
    sources = tool['source']
    sources = list(set([source_match[s] for s in sources]))
    Counts[str(len(sources))] = Counts[str(len(sources))] + 1
    index = len(sources)-1
    for sour in sources:
        if str(len(sources)) not in count_source[sour].keys():
            count_source[sour][str(len(sources))] = 1
        else:
            count_source[sour][str(len(sources))] = count_source[sour][str(len(sources))] + 1

summ=0
for n in range(1,10):
    Counts_cummulative[str(n)]=Counts[str(n)] + summ
    summ += Counts[str(n)]
            
# Building a dictionary     
new_counts = {} # dictionary storing count of instances per source
for s in count_source:
    counts = []
    for num in ['1', '2', '3', '4', '5', '6']:
        n = count_source[s].get(num)
        if n==None:
            n=0
        counts.append(n)
    new_counts[s] = counts

sources_lab ={'github', 'galaxy','bioconda_conda','galaxy_metadata','bioconda', 'toolshed','bioconda_recipes', 'biotools', 'bioconductor', 'sourceforge', 'bitbucket', 'opeb_metrics'}
count_entries = {}
for lab in sources_lab:
    count_entries[lab] = []

pretools = db.pretools
for tool in pretools.find():
    k = tool['source'][0]
    name = tool['name']
    count_entries[k].append(name)

with open('data/intances_sources.json','w') as outfile:
    json.dump(new_counts, outfile)

with open('data/counts_cummulative.json','w') as outfile:
    json.dump(Counts_cummulative, outfile)

for k in count_entries.keys():
    print(f'{k} : {len(count_entries[k])}')
    print(f'{k} : {len(set(count_entries[k]))}')

# Plot
nums=['1', '2', '3', '4', '5']
colors_primary=['#1d4421', '#3d643f', '#5d865e', '#80aa80', '#a3cfa3', '#c9f5c8']
colors_secondary=['#b72f1e', '#f88b69',  '#f9d3c6', '#0a589f']

## Plot settings
fig = go.Figure(data=[
    go.Bar(name='bio.tools', x=nums, y=new_counts['biotools'], marker_color=colors_primary[0]),
    go.Bar(name='Galaxy Europe', x=nums, y=new_counts['galaxy'], marker_color=colors_primary[1]),
    go.Bar(name='Galaxy Toolshed', x=nums, y=new_counts['toolshed'], marker_color=colors_primary[2]),
    go.Bar(name='Bioconda', x=nums, y=new_counts['bioconda'], marker_color=colors_primary[3]),
    go.Bar(name='Bioconductor', x=nums, y=new_counts['bioconductor'], marker_color=colors_primary[4]),
    go.Bar(name='SourceForge', x=nums, y=new_counts['sourceforge'], marker_color=colors_primary[5]),
    go.Bar(name='GitHub', x=nums, y=new_counts['github'], marker_color=colors_secondary[0]),  
    go.Bar(name='Bitbucket', x=nums, y=new_counts['bitbucket'], marker_color=colors_secondary[1]),
    go.Bar(name='OpenEBench', x=nums, y=new_counts['opeb_metrics'], marker_color=colors_secondary[3]),
])

fig.add_trace(
    go.Scatter(
        x=['1', '2', '3', '4', '5'],
        #y=[Counts['1'], Counts['2'], Counts['3'], Counts['4'], Counts['5'], Counts['6'], Counts['7'], Counts['8']],
        y=[Counts_cummulative['1'], Counts_cummulative['2'], Counts_cummulative['3'], Counts_cummulative['4'], Counts_cummulative['5']],
        marker_color='#FFD700',
        name='Instances'
    ))
# Change the bar mode
fig.update_layout(barmode='stack', template='plotly_white', height=400, width=900)
fig.update_xaxes(range=[-1, 5], title_text='Number of sources')
fig.update_yaxes(range=[0, 50000], title_text='Number of entries')
fig.show()
fig.write_image("plots/Supp_Fig2.svg")
fig.write_image("plots/Supp_Fig2.pdf")

types=0
N=0
for tool in tools.find():
    N+=1
    if tool['type']!='unknown':
        types += 1
