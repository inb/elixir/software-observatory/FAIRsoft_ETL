import json

from pymongo import MongoClient

from collections import Counter

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.tools


def features_cummulative():
    counts = []
    for tool in tools.find():
        total_feats = 0
        for feat in tool:
            if feat != '_id' and tool[feat]:
                total_feats += 1
        counts.append(total_feats)

    feat_num = []
    count_cumm = []

    total = 0
    for  number in range(max(set(counts))):
        total += counts.count(number)
        feat_num.append(number)
        count_cumm.append(total)

    output_dic = {
        'feat_num' : feat_num, 
        'count_cumm' : count_cumm
        }

    with open('data/features_cummulative.json', 'w') as fc:
        json.dump(output_dic, fc)
    
    return

def features_xy():
    counts = []
    for tool in tools.find():
        total_feats = 0
        for feat in tool:
            if feat != '_id' and tool[feat]:
                total_feats += 1
        counts.append(total_feats)
    
    counter = Counter(counts)
    dic = {'y':[], 'x':[]}

    for key in counter.keys():
        dic['y'].append(key)
        dic['x'].append(counter[key])
    
    with open('data/features_xy.json', 'w') as fs:
        json.dump(dic, fs)

    return

def types_tools():
    types = {}
    for tool in tools.find():
        if tool['type'] not in types.keys():
            types[tool['type']]=1
        else:
            types[tool['type']]+= 1

    with open('data/types_of_tools.json', 'w') as tts:
        json.dump(types, tts)
    return


#features_cummulative()
#features_xy()
types_tools()