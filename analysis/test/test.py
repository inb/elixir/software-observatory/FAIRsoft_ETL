import unittest
import json 
import warnings

from publications_journal_year_mapp import extract_publications, mappingsGenerator, mappingsGenerator
from publications_journal_year_mapp import build_individual_publications, get_publications_to_journal, frequencies_pubs_tools

class TestPublicationsMappings(unittest.TestCase):

    def test_year_journal_map(self):
        with open('test/data/tools_with_publications.json', 'r') as infile:
            tools = json.load(infile)

        ## --- --- Publications Mappings --- ---
        publications = extract_publications(tools)
        # Getting pmcids
        mappingsG = mappingsGenerator(tools, publications)
        mappingsG.queryPubMedIDs()
        # Querying records
        mappingsG.queryPubRecords()

        ## --- Associating previous information (journal names) to publications in Tools --- 
        ## Register tools and publications 
        ## We bring publications at the tools level rather than at the version, deployment, authority level
        ## to reduce potential biases.
        all_tools = set()
        tools_publications = {}

        ## To optimize the cross-check across different IDs for the same publication, we keep unique entries
        ## for any detected publication
        all_tool, tools_publications, individual_publications = build_individual_publications(tools)
        publications_to_journal = get_publications_to_journal(individual_publications, mappingsG)
        for pub in mappingsG.mapping:
            mappingsG.mapping[pub] = list(mappingsG.mapping[pub])

        # correct output
        asserts_list = [
            {
                "journalTitle" : "Bioinformatics",
                "yearPub" : "2009"
            }
        ]
        self.assertEqual(mappingsG.mapping, asserts_list)


