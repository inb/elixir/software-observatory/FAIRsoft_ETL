import json
import plotly

from pymongo import MongoClient
import plotly.io as pio
from bunch import bunchify
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
tools = db.tools
N_tools = len([tool for tool in tools.find()])
# ### Versioning <a class="anchor" id="versioning"></a>
# #### Versioning calculations <a class="anchor" id="versioning_calc"></a>

V_ss_all = {'Semantic Versioning (X.Y.Z)':0,'Other':0, 'None':0}
other = []
for t in tools.find():
    Vss = {'None':0, 'Semantic Versioning (X.Y.Z)':0, 'Other':0}
    for version in t['version']:
        if version == None:
            Vss['None'] += 1
        elif not version:
            Vss['None'] += 1
        elif len(version)==1 or version  == 'v1':
            Vss['Semantic Versioning (X.Y.Z)'] += 1
        elif len(version.split('.'))>=2:
            Vss['Semantic Versioning (X.Y.Z)'] += 1
        elif version=='unknown':
            Vss['None']
        else:
            Vss['Other'] += 1
            other.append(version)
    if len(t['version'])==0:
        V_ss_all['None'] +=1 
        
    max_key = max(Vss, key=Vss.get)
    V_ss_all[max_key] +=1 

# #### Versioning plots <a class="anchor" id="versioning_plots"></a>

labs=[]
values=[]
for k in V_ss_all.keys():
    values.append(V_ss_all[k])
    if k == 'Semantic Versioning (X.Y.Z)':
        k = 'Semantic Versioning'
    labs.append(k)
    

print(labs)
print(values)



#Generate a color scale
def color_picker(value, N):
    cmap = plt.cm.get_cmap('RdBu')
    rgba = cmap(value/N)
    rgba = tuple(int((255*x)) for x in rgba[0:3])
    rgba = 'rgb'+str(rgba)
    return(rgba)

colors=['#272c47','#5a5f78','#d9d9d9']

fig = go.Figure(data=[go.Pie(labels=labs, 
                             values=values, 
                             hole=.4,
                             textinfo='label+percent',
                             #marker_colors=colors,
                             marker_colors=[color_picker(value,N_tools) for value in values],
                             direction ='clockwise',sort=False)])
fig.update_layout(
    font=dict(
        size=12
    ),
    height=550, width=500
)
fig.show()
fig.write_image("plots/versioning.svg")
fig.write_image("plots/versioning.png")
