import json
import plotly

from pymongo import MongoClient
import plotly.io as pio
from bunch import bunchify
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.test_tools
indicators = db.FAIR_indicators_2

# ### FAIR metrics<a class="anchor" id="fair"></a>
# #### FAIR metrics calculations<a class="anchor" id="fair_data"></a>
'''
metrics_path = '/home/eva/projects/FAIRsoft/FAIRsoft_ETL/FAIRsoft/FAIRsoft/indicators _evaluation/metrics_scores032022.json'
with open(metrics_path, 'r') as metrics_f:
    metrics=json.load(metrics_f)
'''
metrics = list(indicators.find())


metrics_ids = ['F1_1', 'F1_2', 'F2_1', 'F2_2', 'F3_1', 'F3_2', 'F3_3', 
               'A1_1', 'A1_2', 'A1_3', 'A1_4', 'A1_5', 'A2_1', 'A2_2', 'A3_1', 'A3_2', 'A3_3', 'A3_4', 'A3_5', 
               'I1_1', 'I1_2', 'I1_3', 'I1_4', 'I1_5', 'I2_1', 'I2_2', 'I3_1', 'I3_2', 'I3_3', 
               'R1_1', 'R1_2', 'R2_1', 'R2_2', 'R3_1', 'R3_2', 'R4_1', 'R4_2', 'R4_3']

metrics_pos_agg = dict()
metrics_neg_agg = dict()

for i in metrics_ids:
    metrics_pos_agg[i] = 0
    metrics_neg_agg[i] = 0
    
Total = 0    
for instance in metrics:
    Total += 1
    for k in instance.keys():
        if k in metrics_ids:
            if instance[k]==True:
                metrics_pos_agg[k] += 1
            else:
                metrics_neg_agg[k] += 1

print(f"Total:{Total}")
    


# #### Plots<a class="anchor" id="fair_plots"></a>
# ##### Data prep for scores

ids = {'F':['F3','F2', 'F1'],
       'A':['A3', 'A1'],
       'I':['I3', 'I2', 'I1'],
       'R':['R4', 'R3', 'R2', 'R1']
      }

# initializing dict with scores
scores = {}
for p in ids.keys():
    for e in ids[p]:
        scores[e] = []

# populating dict with scores
for inst in metrics:
    for p in ids.keys():
        for e in ids[p]:
            scores[e].append(float(round(inst[e], 2)))

colors = {'F':['#101a4d', '#40407a', '#6e6ba9'],
          'A':['#997f35', '#ccae62'],
          'I':['#005e49','#218c74', '#5abda3'],
          'R':['#4c0000','#7d0013', '#b33939', '#ea6a63']}

labels = {'F':{'F1':'Identity uniqueness',
              'F2':'Existence of Metadata ', 
              'F3':'Searchability'
               },
          'A':{'A1':'Existence of downloadable, <br> buildable or accesible working <br> version', 
              'A3':'Restricted access'
              },
          'I':{'I1':'Documentation on Input/output <br> datatypes and formats ', 
              'I2':'Workflow compatibility', 
              'I3':'Dependencies availability'},
          'R':{'R1':'Existence of usage documentation', 
              'R2':'Existence of License', 
              'R3':'Contributors recognition', 
              'R4':'Provenance availability'}
          }

# ##### Plot scores

labels_y = ['R', 'I', 'A', 'F']
x=[]
colors_ord = []
for p in ['R', 'I', 'A', 'F']:
    n = 0
    for e in ids[p]:
        labels_y.append(labels[p][e])
        x.append(scores[e])
        colors_ord.append(colors[p][n])
        n+=1
        
y=[
    [ 'R', 'R', 'R', 'R', 'I', 'I', 'I',  'A', 'A', 'F', 'F', 'F'],
    labels_y
]

format_ = {'R':"<span style='font-size:300%'> R </span>",
          'I':"<span style='font-size:300%'> I </span>",
          'A':"<span style='font-size:300%'> A </span>",
          'F':"<span style='font-size:300%'> F </span>"}
fig = go.Figure()

for p in ['R', 'I', 'A', 'F']:
    n=0
    for e in ids[p]:
        fig.add_trace(go.Violin(x=scores[e] + [0.0, 1.0],
                                y=[[format_[p]]*len(scores[e]),[labels[p][e]]*len(scores[e])],
                                name=labels[p][e],
                                marker_color = colors[p][n],
                                bandwidth=0.02
                            )
                     )
        n+=1

fig.update_traces(alignmentgroup='metrics', selector=dict(type='violin')) 
fig.update_layout(template='plotly_white', height=1000, width=800, margin = dict(
        l=400,
        r=15,
        b=0,
        t=0,
    ))
fig.update_traces(orientation='h', side='positive', width=1, points=False)
fig.update_layout(xaxis_showgrid=True, xaxis_zeroline=False, showlegend=False)
N=0

for p in ['R', 'I', 'A', 'F']:
    n=0
    for e in ids[p]:
        scores_c = Counter(scores[e])
        print(scores_c)
        for k in dict(scores_c):
            if scores_c[k]/Total>0.3:
                y=0.5
            else:
                y=scores_c[k]/Total
            x=k
            fig.add_annotation(
                x=x,
                y=N+0.2+y,
                xref="x",
                yref="y",
                text="{:.1%}".format(scores_c[k]/Total),
                showarrow=False,
                font=dict(
                    size=12,
                    ),
                align="center",
                )
        N+=1
    n+=1
fig.update_xaxes(title_text="Score", tickfont = dict(size=14))
fig.update_xaxes(showline=True, linewidth=1, gridcolor='#c9c9c9')
fig.write_image("plots/FAIRscores_2.svg")
fig.write_image("plots/FAIRscores_2.pdf")