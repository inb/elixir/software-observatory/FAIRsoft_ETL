import json
import plotly
import itertools

from pymongo import MongoClient
import plotly.io as pio
from bunch import bunchify
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.tools

# ### Licensing <a class="anchor" id="licensing"></a>

# #### License parsing and calculations <a class="anchor" id="licensing_calc"></a>

def map_license(a):
    if 'unlicensed' in a.lower() or 'unknown' in a.lower() or 'unlicense' in a.lower():
        return('unlicensed')
    elif 'agpl' in a.lower() or 'affero' in a.lower():
        return('AGPL')
    elif 'lgpl' in a.lower():
        return('LGPL')
    elif 'gpl' in a.lower() or 'gnu' in a.lower():
        return('GPL')
    elif 'artistic' in a.lower():
        return('artistic')
    elif 'mit' in a.lower():
        return('MIT')
    elif 'apache' in a.lower():
        return('apache')
    elif 'bsd' in a.lower():
        return('BSD')
    elif 'afl' in a.lower():
        return('AFL')
    elif 'CC' in a or 'creative commons' in a.lower():
        return('CC')
    elif 'creativecommons.org' in a.lower():
        return('not-software')
    elif 'cecill' in a.lower():
        return('CeCILL')
    else:
        return('other')
    

#-------------------------------------------------
# Licensing that are equal to None
#-------------------------------------------------
'''
import collections
none = 0
licenses_mapped = []
other = set()
n=0
c=0
prob = 0
for i in tools.find():
    n+=1
    licenses = i['license']
    if None in licenses:
        licenses.remove(None)

    if licenses == []:
        none += 1
        continue
    else:
        c+=1
        lic_maps=set()
        for lic in licenses:
            try:
                licenses_mapped.append(map_license(lic))
                if map_license(lic) == 'other':
                    other.add(lic)
                    
            except:
                prob+=1
                print(lic)

print(collections.Counter(licenses_mapped))
for i in other:
    print(i)
print(prob)
'''
coincidental_lics=[]
conflicting_lics=[]
none=0
not_soft=0
n=0
c=0


for i in tools.find():
    n+=1
    licenses = i['license']
    if None in licenses:
        licenses.remove(None)
        
    if licenses == []:
        none += 1
        continue
    else:
        c+=1
        lic_maps=set()
        for lic in licenses:
            if type(lic) == list:
                for l in lic:
                    try:
                        lic_maps.add(map_license(l))
                    except:
                        print(l)
            else:
                try:
                    lic_maps.add(map_license(lic))
                except:
                    print(lic)
                    
        # License is sometimes inside the documentation as a file
        for doc in i['documentation']:
            if 'license' in doc[0].lower():
                try:
                    lic_maps.add(map_license(lic))
                except:
                    print(lic)
                

        #print(lic_maps)
        if 'opeb_metrics' in i['source'] and 'not-software' in lic_maps:
            lic_maps.remove('not-software')
        
        if len(lic_maps)==1 and 'unlicensed' in lic_maps:
            none+=1

        elif len(lic_maps)==1:
            coincidental_lics.append(lic_maps)

        elif len(lic_maps)==2 and 'unlicensed' in lic_maps:
            coincidental_lics.append(lic_maps)    
 
        elif len(lic_maps)>1:
            conflicting_lics.append(lic_maps)
        else:
            none+=1

conflict_lics = [len(s) for s in conflicting_lics]
other_other_lics = len([l for l in conflicting_lics if 'other' in l])
count_lics = Counter(conflict_lics)

license_summary = {'Total':n, 'None': none,'Unambiguous':len(coincidental_lics), 'Ambiguous':len(conflicting_lics)}
print(license_summary)

# #### License plots 
list_coinc=[]
for instance in coincidental_lics:
    found=False
    for lic in list(instance):
        if found == True:
            continue
        elif lic!='unlicensed':
            list_coinc.append(lic)
            found = True
            
count_unambiguous=Counter(list_coinc)
count_unambiguous=dict(count_unambiguous)
count_unambiguous['opensource']=len(list_coinc)-count_unambiguous['other']
print(count_unambiguous)
# Sunburst plot plus licenses type

data = pd.DataFrame({'ids':["Total", 
                            'No license stated', 
                            'Unambiguous', 
                            'Open Source',
                            'other',
                            'Ambiguous',
                            'BSD', 
                            'GPL', 
                            'MIT', 
                            'Artistic', 
                            'LGPL', 
                            'Apache', 
                            'other OS'],
                     'parents':['',
                                'Total',
                                'Total',
                                'Unambiguous',
                                'Unambiguous',
                                'Total',
                                'Open Source',
                                'Open Source',
                                'Open Source',
                                'Open Source',
                                'Open Source',
                                'Open Source',
                                'Open Source'],
                     'v':[license_summary['Total'], 
                          license_summary['None'], 
                          license_summary['Unambiguous'],
                          count_unambiguous['opensource'],
                          count_unambiguous['other'],
                          license_summary['Ambiguous'],
                          count_unambiguous['BSD'], 
                          count_unambiguous['GPL'], 
                          count_unambiguous['MIT'], 
                          count_unambiguous['artistic'], 
                          count_unambiguous['LGPL'], 
                          count_unambiguous['apache'], 
                          count_unambiguous['CC']+ 
                          count_unambiguous['AGPL']+ 
                          count_unambiguous['CeCILL']+ 
                          count_unambiguous['AFL']]})

values_colorscale = ['#3b475c', '#5c6577', '#7f8594', '#a3a7b1', '#c8cacf', '#eeeeee', '#fbbea5', '#f6855d', '#dc4e2d', '#ac2513', '#770000']
fig = go.Figure()
fig.add_sunburst(
        labels = data.ids,
        parents=data.parents,
        values=data.v,
        branchvalues='total',
        textinfo="label+percent parent",
        marker=dict(
            colorscale="RdBu",
            colorbar=dict(
                ticks="outside",
                len=0.65,
                y=0.6,
                x=1.1,
                outlinewidth=0,
                separatethousands=True,
                tickformat=",d",
                tickfont=dict(size=14)
                )
            ),
        rotation=152
    )

fig.update_layout(
    font=dict(
        size=10
    ),
    height=650, width=750,
)
fig.show()
fig.write_image("plots/licensing2.svg")
fig.write_image("plots/licensing2.png")

