import json
import plotly

from pymongo import MongoClient
import plotly.io as pio
from bunch import bunchify
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.tools

# # Features
### --- Labels and equivalences of labels 

# Features' names in original data
features_names = ['name', 'description', 'version', 'type', 'links', 'publication', 'download',
                        'inst_instr', 'test', 'src', 'os', 'input', 'output', 'dependencies', 'documentation',
                        'license', 'authors', 'repository','citations','bioschemas','ssl','operational']

# Equivalence of fields in original data and labels in plot X axis. Must be the same as in `fps`!!
feat_labels = {'name':'Name', 
                'description':'Description', 
                'version':'Version', 
                'type':'Type', 
                'links':'Links', 
                'publication':'Publication', 
                'download':'Download',
                'inst_instr':'Installation instructions', 
                'test':'Test', 
                'src':'Source code', 
                'os':'Operating system', 
                'input':'Input format', 
                'output':'Output format', 
                'dependencies':'Dependencies', 
                'documentation':'Documentation',
                'license':'License', 
                'authors':'Authors', 
                'repository':'Repository',
                'citations':'Historical number of citations',
                'bioschemas':'Bioschemas',
                'ssl':'SSL',
                'operational':'Historical homepage accessibility'
}
### ---

## Data in scatter plot (bullets indicating availability of data from sources)
fps={"sources": ["Bioconda" ,
                "Bioconductor", 
                "bio.tools", 
                "Galaxy Toolshed",
                "Galaxy Europe", 
                "SourceForge", 
                "Bitbucket", 
                "Github", 
                "OpenEBench"],
    "data":{"Name" : [1,1,1,1,1,1,1,1, 0],
            "Description": [2,2,2,2,2,2,2,2, 0],
            "Version": [3,3,3,3,3,3,3,3,0],
            "Type": [4,4,4,4,4,0,0,0,0],
            "Links": [5,5,5,5,5,5,5,5,0],
            "Publication":[6,6,6,6,6,0,0,0,0],
            "Download":[7,7,7,7,0,7,7,7,0],
            "Installation instructions": [8,8,8,8,0,8,8,8,0],
            "Test": [0,0,0,9,0,0,0,0,0],
            "Source code":[10,10,10,10,0,10,10,10,0],
            "Operating system":[11,11,11,11,11,11,0,0,0],
            "Input format":[0,0,12,12,0,0,0,0,0],
            "Output format":[0,0,13,13,0,0,0,0,0],
            "Dependencies":[0,14,0,14,0,0,0,0,0],
            "Documentation":[0,15,15,15,0,0,15,15,0],
            "License": [16,16,16,16,16,16,16,16,0],
            "Authors":[0,17,17,17,0,0,17,17,0],
            "Repository":[18,18,18,18,18,18,18,18,0],
            "Historical number of citations":[0,0,0,0,0,0,0,0,19],
            "Bioschemas":[0,0,0,0,0,0,0,0,20],
            "SSL":[0,0,0,0,0,0,0,0,21],
            "Historical homepage accessibility":[0,0,0,0,0,0,0,0,22]}}

## ------------------- DATA --------------------------------------

def exist_citations(tool):
    if 'opeb_metrics' in tool['source']:
        if tool.get('publication'):
            for pub in tool['publication']:
                if pub.get('citations'):
                    return(1)
    return(0)

def exist_operational(tool):
    if 'opeb_metrics' in tool['source']:
        return(1)
    else:
        return(0)

# unique names with citations
cts_tools = set()
cts_t=set()
for tool in tools.find():
    if exist_citations(tool):
        cts_tools.add(tool['name'])
    if tool["publication"]:
        cts_t.add(tool['name'])
'''   
'''
print('Number of unique tool names with citation information: %d'%len(cts_tools))
print(len(cts_t))

def count_opeb_metrics():
    citations = 0
    operational = 0
    for tool in tools.find():
        citations += exist_citations(tool)
        operational += exist_operational(tool)
    return({'citations': citations, 'operational':operational, 'ssl':operational, 'bioschemas':operational})

def update_dict(tool, count_dict, feature):
    if tool.get(feature):
        if type(tool.get(feature)) == list:
            #print(tool.get(feature))
            for a in tool.get(feature):
                if a != 'unknown':
                    if a != 'None':
                        if a != None:
                            count_dict[feature] +=1
                            return(count_dict)
        else:
            a = tool.get(feature)
            if a != None:
                if a != 'unknown':
                    if a != 'None':
                        count_dict[feature] +=1
                        return(count_dict)

    return(count_dict)

def features():
    features_dict={}
    count_features = {label:0 for label in features_names}
    for tool in tools.find():
        for feat in features_names:
            if feat not in ['citations','bioschemas','ssl','operational']:
                count_features = update_dict(tool, count_features, feat)
    dict_metrics=count_opeb_metrics()
    count_features={**count_features, **dict_metrics}
    for name in features_names:
        features_dict[feat_labels[name]]=count_features[name]

    print(features_dict)

    with open('data/features.json', 'w') as fs:
        json.dump(features_dict, fs)

    return(features_dict)

features()

def features_count_sources():
    # Two subplots in the figure (scatter + bar)
    fig_feat_s = make_subplots(rows=2, cols=1,
                                specs=[[{"rowspan": 1, "colspan": 1}],
                                        [{"rowspan": 1, "colspan": 1}]],
                                vertical_spacing = 0)
    ## --- scatter subplot (upper) ----
    # Structuring data to feed the plot
    scatter_data = {'source' : fps['sources'],
                    'values' : [fps['data'][feat_labels[lab]] for lab in features_names]
                    }

    # Addding the scatter plot (line of bullets) for each source
    for feats_availability in scatter_data['values']:
        scatter_plot = go.Scatter(x=feats_availability, 
                                  y=scatter_data['source'], 
                                  mode = "markers", 
                                  marker=dict(size=8, 
                                              color=['#7DC370', 
                                                    '#7DC370', 
                                                    '#7DC370', 
                                                    '#7DC370', 
                                                    '#7DC370',
                                                    '#7DC370', 
                                                    '#ff6641', 
                                                    '#ff6641', 
                                                    '#5a81a6'],
                                              line=dict(width=2,
                                                        color=['#4D9243',
                                                               '#4D9243',
                                                               '#4D9243',
                                                               '#4D9243',
                                                               '#4D9243',
                                                               '#4D9243',
                                                               '#B7482D',
                                                               '#B7482D',
                                                               '#0A589F'])
                                             ),
                                  hoverinfo='none')
        fig_feat_s.add_trace(scatter_plot, row=1, col=1) # upper subplot

    #x-axis style
    fig_feat_s.update_xaxes(showticklabels=False, 
                            range=[0.5, 22.5],
                            row=1, col=1)

    # ---- bar subplot (lower) ----------
    ## Structring data as a list of points (x,y) to feed the plot
    bar_plot_counts = features()
    bars_data = [[k,bar_plot_counts[k]] for k in bar_plot_counts]
    
    # Adding bar plot of counts of tools vs features 
    trace = go.Bar(
        x=[f[0] for f in bars_data],
        y=[f[1] for f in bars_data],
        name='All',
        marker_color='#3B475C',
        text = [f[1] for f in bars_data],
        texttemplate='%{text:,.0f}', 
        textposition='outside',
        textfont=dict(
            family="Balto, sans-serif",
            size=9,
            color='#3B475C'
        )
    )

    fig_feat_s.add_trace(trace, row=2, col=1) # lower subplot

    #axes style
    fig_feat_s.update_xaxes(title_text='Feature', 
                            tickangle=-45, 
                            row=2, 
                            col=1)

    N_tools = len([a for a in tools.find()])

    fig_feat_s.update_yaxes(title_text='Number of instances', 
                            row=2, 
                            col=1, 
                            range=[0, N_tools +6000]) #+6000 additional vspace for annotations

    #----- general layout -------
    fig_feat_s.layout.template = 'plotly_white'
    fig_feat_s.update_layout(showlegend=False,
                            height=500,
                            width=1000,
                            font_size=12,
                            font_color = 'black',
                            font_family="Balto, sans-serif",
                            margin=dict(t=0, b=0, l=0, r=0),
                            bargap=0.1,bargroupgap=0.0)

    return(fig_feat_s)

fig = features_count_sources()
fig.show()
fig.write_image("plots/Fig1.svg")
fig.write_image("plots/Fig1.png")
fig.write_image("plots/Fig1.pdf")

