import json

from pymongo import MongoClient

from collections import Counter

# connecting to DB
client = MongoClient(port=27017)
db = client.FAIRsoft
alambique = db.alambique
tools = db.tools


# ### Licensing <a class="anchor" id="licensing"></a>

# #### License parsing and calculations <a class="anchor" id="licensing_calc"></a>

def map_license(a):
    if 'unlicensed' in a.lower() or 'unknown' in a.lower() or 'unlicense' in a.lower():
        return('unlicensed')
    elif 'agpl' in a.lower() or 'affero' in a.lower():
        return('AGPL')
    elif 'lgpl' in a.lower():
        return('LGPL')
    elif 'gpl' in a.lower() or 'gnu' in a.lower():
        return('GPL')
    elif 'artistic' in a.lower():
        return('artistic')
    elif 'mit' in a.lower():
        return('MIT')
    elif 'apache' in a.lower():
        return('apache')
    elif 'bsd' in a.lower():
        return('BSD')
    elif 'afl' in a.lower():
        return('AFL')
    elif 'CC' in a or 'creative commons' in a.lower():
        return('CC')
    elif 'creativecommons.org' in a.lower():
        return('not-software')
    elif 'cecill' in a.lower():
        return('CeCILL')
    else:
        return('other')
    
    
coincidental_lics=[]
conflicting_lics=[]
none=0
not_soft=0
n=0
c=0
 
for i in tools.find():
    n+=1
    licenses = i['license']
    if None in licenses:
        licenses.remove(None)
        
    if licenses == []:
        none += 1
        continue
    else:
        c+=1
        lic_maps=set()
        for lic in licenses:
            try:
                lic_maps.add(map_license(lic))
            except:
                print(lic)

        #print(lic_maps)
        if 'opeb_metrics' in i['source'] and 'not-software' in lic_maps:
            lic_maps.remove('not-software')
        
        if len(lic_maps)==1 and 'unlicensed' in lic_maps:
            none+=1

        elif len(lic_maps)==1:
            coincidental_lics.append(lic_maps)

        elif len(lic_maps)==2 and 'unlicensed' in lic_maps:
            coincidental_lics.append(lic_maps)    
 
        elif len(lic_maps)>1:
            conflicting_lics.append(lic_maps)
        else:
            none+=1

conflict_lics = [len(s) for s in conflicting_lics]
other_other_lics = len([l for l in conflicting_lics if 'other' in l])
count_lics = Counter(conflict_lics)

license_summary = {'Total':n, 'None': none,'Unambiguous':len(coincidental_lics), 'Ambiguous':len(conflicting_lics)}
license_summary

# #### License plots <a class="anchor" id="licensing_plots"></a>

list_coinc=[]
for instance in coincidental_lics:
    found=False
    for lic in list(instance):
        if found == True:
            continue
        elif lic!='unlicensed':
            list_coinc.append(lic)
            found = True
            
count_unambiguous=Counter(list_coinc)
count_unambiguous=dict(count_unambiguous)
count_unambiguous['opensource']=len(list_coinc)-count_unambiguous['other']

# Sunburst plot plus licenses type

data = {'ids':["Total", 
            'No license stated', 
            'Unambiguous', 
            'Open Source',
            'other',
            'Ambiguous',
            'BSD', 
            'GPL', 
            'MIT', 
            'Artistic', 
            'LGPL', 
            'Apache', 
            'other OS'],
        'parents':['',
                'Total',
                'Total',
                'Unambiguous',
                'Unambiguous',
                'Total',
                'Open Source',
                'Open Source',
                'Open Source',
                'Open Source',
                'Open Source',
                'Open Source',
                'Open Source'],
        'v':[license_summary['Total'], 
            license_summary['None'], 
            license_summary['Unambiguous'],
            count_unambiguous['opensource'],
            count_unambiguous['other'],
            license_summary['Ambiguous'],
            count_unambiguous['BSD'], 
            count_unambiguous['GPL'], 
            count_unambiguous['MIT'], 
            count_unambiguous['artistic'], 
            count_unambiguous['LGPL'], 
            count_unambiguous['apache'], 
            count_unambiguous['CC']+ 
            count_unambiguous['AGPL']+ 
            count_unambiguous['CeCILL']+ 
            count_unambiguous['AFL']]}

data_bars = {'BSD': count_unambiguous['BSD'], 
            'GPL':count_unambiguous['GPL'], 
            'MIT':count_unambiguous['MIT'], 
            'artistic':count_unambiguous['artistic'], 
            'LGPL':count_unambiguous['LGPL'], 
            'apache':count_unambiguous['apache'], 
            'CC':count_unambiguous['CC'],
            'AGPL':count_unambiguous['AGPL'], 
            'CeCILL':count_unambiguous['CeCILL'], 
            'AFL':count_unambiguous['AFL']}


with open('data/licenses_pie.json', 'w') as fc:
    json.dump(data, fc)

with open('data/licenses_bars.json', 'w') as fc:
    json.dump(data_bars, fc)

V_ss_all = {'Semantic Versioning (X.Y.Z)':0,'Other':0, 'None':0}
other = []
for t in tools.find():
    Vss = {'None':0, 'Semantic Versioning (X.Y.Z)':0, 'Other':0}
    for version in t['version']:
        if version == None:
            Vss['None'] += 1
        elif not version:
            Vss['None'] += 1
        elif len(version)==1 or version  == 'v1':
            Vss['Semantic Versioning (X.Y.Z)'] += 1
        elif len(version.split('.'))>=2:
            Vss['Semantic Versioning (X.Y.Z)'] += 1
        elif version=='unknown':
            Vss['None']
        else:
            Vss['Other'] += 1
            other.append(version)
    if len(t['version'])==0:
        V_ss_all['None'] +=1 
        
    max_key = max(Vss, key=Vss.get)
    V_ss_all[max_key] +=1 

# #### Versioning plots <a class="anchor" id="versioning_plots"></a>

labs=[]
values=[]
for k in V_ss_all.keys():
    values.append(V_ss_all[k])
    if k == 'Semantic Versioning (X.Y.Z)':
        k = 'Semantic Versioning'
    labs.append(k)
    
data = {
    'labels':labs,
    'values':values
}


with open('data/versioning_plot.json', 'w') as fc:
    json.dump(data, fc)


def version_control_count():
    ## repos
    c_github = 0
    c_gitlab = 0
    c_bitbucket = 0
    c_sourceforge = 0
    no_repo = 0
    repo = 0
    P = False
    for e in tools.find():
        links = []
        flag = False
        if e['repository']:
            for a in e['repository']:
                links.append(a)

        if flag==False:
            if e['links']:
                for w in e['links']:
                    links.append(w)
        
        R = False
        links = list(set(links))
        git_c=0
        gitl_c=0
        bit_c=0
        sc_c=0

        for r in links:
            if 'github.com' in r:
                git_c = 1
                R=True
            elif 'bitbucket' in r:
                bit_c = 1
                R=True
            elif 'sourceforge' in r:
                sc_c = 1
                R=True
            elif 'gitlab' in r:
                gitl_c = 1
                R=True


        c_github += git_c
        c_gitlab += gitl_c
        c_bitbucket += bit_c
        c_sourceforge += sc_c

        if R == False:
            no_repo += 1
        else:
            repo += 1


    vc_general = {
        'version control' : repo, 
        'no version control': no_repo,
    }
    with open('data/version_control.json', 'w') as vcs:
        json.dump(vc_general, vcs)


    repositories = {
        'github' : c_github,
        'bitbucket' : c_bitbucket,
        'sourceforge' : c_sourceforge,
        'gitlab' : c_gitlab  
    }
    with open('data/repositories_sp.json', 'w') as rs:
        json.dump(repositories, rs)

    return

version_control_count()
