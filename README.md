# Software metadata extraction and consolidation pipeline for FAIRness evaluation

<img align="right" width="150" src="./diagram.svg">

[*FAIRsoft*](https://inab.github.io/FAIRsoft_indicators/) are a set of research software *FAIRness* indicators, specifically devised to be assesed automatically. In order to test them, we have developed a pipeline to gather metadata about research software specific to Computational Biology, harmonize and integrate it and then perform an evaluation of their compliance with *FAIRsoft* indicators. 

This repository contains the code relative to the: 
- Extraction of software metadata from Bioconda, Bioconductor, GalaxyToolshed, bio.tools, OpenEBench, SourceForge, Bitbucket and GitHub. 
- Harmonization of the previously obtained metadata.
- Integration of pieces of metadata belonging to the same software.
- Calculation of *FAIRsoft* indicators compliance and FAIRsoft scores.



## Configuration 
Metadata is stored in a Mongo Database. The database connection is configured through environment variables explained [here](https://gitlab.bsc.es/inb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft). 

## Installation 

Install the [FAIRsoft](https://pypi.org/project/FAIRsoft/) package using pip:

```sh
pip install FAIRsoft
```

> ❗️ There are further requirements to run some importers.  **Bioconda**, **Galaxy Toolshed** and **repositories** (GitHub and Bitbucket) metadata importers require the installation of **additional software** detailed in the [FAIRsoft documentation](https://gitlab.bsc.es/inb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft). 


## Data Extraction 
Data extraction is done through the execution of importers. Each importer is responsible for extracting metadata from a specific source. 

### Docker images  
Given the complexity of the installation of the requirements of some of the importers, it is highly recommended to use the dockerized version of the different importers:

- [Bioconda importer](https://gitlab.bsc.es/inb/elixir/software-observatory/bioconda-importer): `registry.bsc.es/inb/elixir/software-observatory/bioconda-importer`
- [Bioconductor importer](https://gitlab.bsc.es/inb/elixir/software-observatory/bioconductor-importer): `registry.bsc.es/inb/elixir/software-observatory/bioconductor-importer`
- [Galaxy Toolshed importer](https://gitlab.bsc.es/inb/elixir/software-observatory/toolshed-importer): `registry.bsc.es/inb/elixir/software-observatory/toolshed-importer`
- [OpenEBench tools importer](https://gitlab.bsc.es/inb/elixir/software-observatory/opeb-tools-importer): `registry.bsc.es/inb/elixir/software-observatory/opeb-tools-importer`
- [OpenEBench metrics importer](https://gitlab.bsc.es/inb/elixir/software-observatory/opeb-metrics-importer): `registry.bsc.es/inb/elixir/software-observatory/opeb-metrics-importer`
- [Sourceforge importer](https://gitlab.bsc.es/inb/elixir/software-observatory/sourceforge-importer): `registry.bsc.es/inb/elixir/software-observatory/sourceforge-importer`
- [Repositories importer](https://gitlab.bsc.es/inb/elixir/software-observatory/respositories-importer): `registry.bsc.es/inb/elixir/software-observatory/respositories-importer` 

> 💡 See details on configuration in the documentation of each importer. 

### Native execution 

Importers can also be executed natively using the [FAIRsoft](https://pypi.org/project/FAIRsoft/) package. 

To execute the importers, after installing the requirements, use the following commands:

```sh
FAIRsoft_bioconda_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_bioconductor_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_toolshed_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_opeb_tools_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_opeb_metrics_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_sourceforge_importer -e=[env-file] -l=[log-level] -d=[log-directory]
FAIRsoft_repositories_importer  
```
- `-e`/`--env-file` is optional. It specifies the path to the file containing the environment variables. Default is `.env`.
- `-l`/`--loglevel` is optional. It can be `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`. Default is `INFO`.
- `-d`/`--logdir`/ is optional. It specifies the path to the directory where the logs will be written. Default is `./logs`.


> 💡 In addition to database connection details mentioned before, some other variables can be overriden by the user. See details on configuration in [FAIRsoft documentation](https://gitlab.bsc.esnb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft). 


## Data Transformation

### Docker image

- [Transformation](https://gitlab.bsc.es/inb/elixir/software-observatory/fairsoft-transformation): `registry.bsc.es/inb/elixir/software-observatory/fairsoft-transformation` 

### Native execution 

The only requirement to execute the transformation is [FAIRsoft](https://pypi.org/project/FAIRsoft/) package.

Use the following command to transform the data:
```sh
FAIRsoft_transform --env-file=[env-file] -l=[log-level] 
```

- `-e`/`--env-file` is optional. It specifies the path to the file containing the environment variables. Default is `.env`.
- `-d`/`--logdir`/ is optional. It specifies the path to the directory where the logs will be written. Default is `./logs`.

> 💡 See details on configuration in [FAIRsoft documentation](https://gitlab.bsc.esnb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft). 


## Data integration

### Docker image
- [Integration](https://gitlab.bsc.es/inb/elixir/software-observatory/fairsoft-integration): `registry.bsc.es/inb/elixir/software-observatory/fairsoft-integration`

### Native execution 

The only requirement to execute the transformation is [FAIRsoft](https://pypi.org/project/FAIRsoft/) package.

Use the following command to integrate the data:
```sh
FAIRsoft_integrate --env-file=[env-file] -l=[log-level] 
```

- `-e`/`--env-file` is optional. It specifies the path to the file containing the environment variables. Default is `.env`.
- `-l`/`--loglevel` is optional. It can be `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`. Default is `INFO`.

> 💡 See details on configuration in [FAIRsoft documentation](https://gitlab.bsc.esnb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft). 

## FAIRsoft indicators evaluacion 

The only requirement to execute the transformation is [FAIRsoft](https://pypi.org/project/FAIRsoft/) package.

Use the following command to evaluate the FAIRsoft indicators:
```sh
FAIRsoft_indicators_evaluation --env-file=[env-file] -l=[log-level]
```

> 💡 See details on configuration in [FAIRsoft documentation](https://gitlab.bsc.esnb/elixir/software-observatory/FAIRsoft_ETL/-/tree/master/FAIRsoft).
