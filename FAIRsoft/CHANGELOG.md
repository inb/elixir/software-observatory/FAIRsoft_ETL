# Changelog

All notable changes to this project will be documented in this file. 
This format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), 

### Added 

### Changed

- Evaluation of documentation adapted to new schema.
- Evaluation of license information adapted to new schema.
- Evaluation of input and output data formats adapted to new schema.
- Evaluation of usage of controlled vocabularies and/ontologies adapted to new schema.
