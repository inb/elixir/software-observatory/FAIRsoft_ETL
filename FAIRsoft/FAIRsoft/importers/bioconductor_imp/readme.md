# Bioconductor importer 


## Set-up and Usage

```sh
FAIRsoft_import_bioconductor -l=[log-level] -d=[log-directory]
``` 
`log_level` can be `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`. Default is `INFO`.
`log_directory` is the path to the directory where the log file will be stored. Default is `./logs/`.


### Environment variables 
They can be set in an `.env` file if importer is running nativelly or in the `docker-compose.yml`, if it is running inside a contianer.

| Name             | Description | Default | Notes |
|------------------|-------------|---------|-------|
| STORAGE_MODE     |  Specifies whether the output will be stored in filesystem (`filesystem`) or pushed to a database (`db`) |  `db` |            |
| DBHOST       |  Host of database where output will be pushed |   `localhost`        |  Only required when STORAGE_MODE is `db`      |
| DBPORT       |  Port of database where output will be pushed |   `27017`            |  Only required when STORAGE_MODE is `db`      |
| DB         |  Name of database where output will be pushed |   `observatory`      |  Only required when STORAGE_MODE is `db`      |
| ALAMBIQUE |  Name of database where output will be pushed |   `alambique`        |  Only required when STORAGE_MODE is `db`      |
| OUTPUT_PATH      |  Path to output file                          | `./data/bioconductor.json` |  Only required when STORAGE_MODE is `filesystem` | 
| REMOTE | Whether the selenium web driver is running in a remote machine. |  |  Always required. If `False`, native selenium is used instead of a remote Selenium Grid | 
| HOST | Hpst name of machine running selenium. | `localhost` | Only required if REMOTE is `True`.  <br />If importer runs in container, HOST is the name of Selenium Hub service (`selenium-hub`, for example).  <br />If importer runs natively, HOST is `localhost`. | 
| PACKAGES_URLS_PATH | Path to file containing the URLs of the packages to be scraped. | `./data/bioconductor_opeb.txt` |  |
