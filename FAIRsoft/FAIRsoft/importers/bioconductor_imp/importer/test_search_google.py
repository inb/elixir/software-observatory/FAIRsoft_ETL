from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from dotenv import load_dotenv

import os


import time
def init_driver(host='localhost'):
    '''
    Initialize the remote driver
    added arguments are not required unless you are running the script in a docker container
    '''

    print('Initializing driver')
    options = webdriver.FirefoxOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument('--disable-dev-shm-usage')
    
    if host != 'localhost':
        time.sleep(15) # wait for selenium-hub to be ready

    driver = webdriver.Remote(
            command_executor=f'http://{host}:4444/wd/hub',
            options=options,
            )
    return driver

def init_driver_local():
    '''
    Initialize the local driver
    '''
    print('Initializing driver')
    options = webdriver.FirefoxOptions()
    driver = webdriver.Firefox(options=options) # for local execution instead of selenium grid
    return driver

class Driver:
    def __init__(self, host):
        self.driver = init_driver(host)
        self.counter = 0

    def add_one(self):
        self.counter += 1

def search_google(driver_inst,search_term):
    '''
    Search google for the word "ChromeDriver"
    '''

    try:
        driver = driver_inst.driver
        driver.get("http://www.google.com")
        print('Page loaded')
        print(f"Page title {driver.title}")
        
        if driver_inst.counter == 0:
            # Reject cookies the first time
            WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="W0wltc"]'))).click()

        # Identify search box
        box = WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')))

        # Fill search box
        box.send_keys(search_term)
        print('Search box filled')

        # Submit search box
        box.send_keys(Keys.RETURN)
        print('Search box submitted')

        # Wait for results
        link = WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.TAG_NAME, 'h3')))
        # Print first result
        print('Results found')
        print(f"Link text {link.text}")

    except Exception as e:
        print('Search failed')
        print(e)
        driver.quit()

if __name__ == '__main__':
    load_dotenv()
    HOST = os.getenv('HOST')
    if HOST:
        print(f'Host is set to {HOST}')
    else:
        print('Host is not set')
        exit(
            'Please set the HOST environment variable to the IP address of the machine running the selenium-hub container. If you are running the script locally, set HOST to the IP address of the current machine'
        )

    driver_inst = Driver(host=HOST)
    search_google(driver_inst,'ChromeDriver')
    driver_inst.add_one()
    search_google(driver_inst,'Selenium')
    driver_inst.add_one()
    search_google(driver_inst,'Python')
    driver_inst.driver.quit()