from bs4 import BeautifulSoup
from bs4.dammit import EncodingDetector
import requests
import logging

from utils import getHTML


def get_whole_table():
    table_url = "https://bioconductor.org/packages/release/bioc/"
    packages_lst_html = getHTML(table_url) # bs4 object

    table = packages_lst_html.find("div", {"class":"do_not_rebase"})
    if len(table)>1:
        logging.warning("more than one table found")
        
    return(table)


def extract_name_url(table):
    bioc_url= 'https://bioconductor.org/packages/release/bioc/'
    all_packages = []
    for tag in table.tbody:
        a = tag.find('a')
        if a and a!=-1:
            #name = a.get_text()
            url = bioc_url + a['href']
            all_packages.append(url)
    
    return(all_packages)


def fetch_all_packages():
    table = get_whole_table()
    all_packages = extract_name_url(table)
    return(all_packages)

if __name__ == "__main__":
    packs = fetch_all_packages()
    file_path = "tmp/list_packages.txt"
    with open(file_path, "w") as outfile:
        logging.debug(f"Writing URLs of packages to file {file_path}")
        x = 1
        for p in packs:
            logging.debug(f"Package {n} of {len(packs)}")
            outfile.write("{p}\tNA\n")
            x += 1
