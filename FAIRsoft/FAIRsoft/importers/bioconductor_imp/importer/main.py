import argparse
import json
import re
import os
import time
import datetime
import logging

from selenium import webdriver
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.keys import Keys
from dotenv import load_dotenv

from list_packages import fetch_all_packages
from utils import getHTML, push_entry, save_entry, connect_db
from publication_extraction import get_publication

# 1 - Get arguments
def get_program_arguments(arguments_parser): 
    arguments = arguments_parser.parse_args()
    return(arguments)

def program_arguments_parser():
    parser = argparse.ArgumentParser(
            prog='',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--enricher', 
        dest='enricher', 
        action='store_true', 
        help='Whether tools whose metadata is retrieved are provided in a file (enricher mode) or not (importer mode). If not set, all metadata of all tools in Bioconductor is extracted (pure importer option). If set, variables "PACKAGES_URLS_PATH" must be set.'
        )
    parser.add_argument(
        "--env-file", "-e",
        help=("File containing environment variables to be set before running "),
        default=".env",
    )
    parser.add_argument(
        "--loglevel", "-l",
        help=("Set the logging level"),
        required=False,
        default="INFO",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        )
    parser.add_argument(
        "--logdir", "-d",
        help=("Set the logging directory"),
        default="./logs/summary.log",
    )    

    return(parser)


# 3 - Driver
class Driver:
    '''
    This class initializes the driver. 
    It can be used to initialize a local driver or a remote driver.
    Arguments:
    - host: the host where the driver is running (default: localhost)
    - remote: boolean, True if the driver is running on a remote host, False otherwise
    '''
    def __init__(self, host, remote=False):
        if remote==False:
            self.driver = self.init_driver_local()
        else:
            self.driver = self.init_driver(host)

        self.counter = 0 # counter for the number of pages visited. Used to know when to deal with cookies dialogs
        logging.info('driver initialized')

    def add_one(self):
        self.counter += 1

    def init_driver_local(self):
        '''
        Initialize the local driver
        '''
        logging.debug('initializing driver')
        options = webdriver.FirefoxOptions()
        driver = webdriver.Firefox(options=options) # for local execution instead of selenium grid
        logging.debug('driver initialized with host localhost')
        return driver
    
    def init_driver(self,host='localhost'):
        '''
        Initialize the selenium webdriver
        added arguments are not required unless you are running
        the code in a docker container
        '''
        logging.debug('initializing driver at host: {host}')
        options = webdriver.FirefoxOptions()
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-dev-shm-usage')

        if host != 'localhost':
            time.sleep(20) # wait for selenium-hub to be ready (20 seconds is arbitrary). 

        driver = webdriver.Remote(
            command_executor=f'http://{host}:4444/wd/hub',
            options=options,
            )
        
        logging.debug(f'driver initialized with host {host}')
        return(driver)


def preParsing(bs):
    '''
    This function returns name, existence of citation and two parts of the bsObject for further parsing.
    part 1: extended name, description, authors, maintainers and citation
    part 2: a dictionary with all fragments under a <h3>.
    '''
    # HTML is parsed in order of appearance of tags

    # Getting name
    Name = bs.find('h1').get_text()

    # Getting first part
    html1 = []
    for tag in bs.find('h2').next_siblings:
        if tag.name == 'h3':
            name = tag.contents[0]
            break
        else:
            html1.append(tag)

    # Getting existence of citation
    if bs.find('div', attrs={"class":"bioc_citation"}):
        citation = 1
    else:
        citation = 0

    # Getting second part
    html2 = {}
    htmlsub2 = []
    for tag in bs.find('h3').next_siblings:
        if tag.name == 'h3':
            html2[name] = htmlsub2
            htmlsub2 = []
            name = tag.contents[0]
        else:
            htmlsub2.append(tag)
    html2[name] = htmlsub2

    return(Name, citation, html1, html2)

def getInsDocDetArch(part2):
    instRaw = part2.get('Installation', None)
    docRaw = part2.get('Documentation', None)
    detailsRaw = part2.get('Details', None)
    packRaw = part2.get('Package Archives', None)

    return(instRaw, docRaw, detailsRaw, packRaw)


def parsePart1(bs, items):
    ps = [p for p in bs if p.name == 'p' ]
    if 'Bioconductor version' in ps[0].contents[0]:
        items['description'] = ps[1].contents[0]
        Auth = [a for a in ps[2].contents[0].split(':')[1].split(',') ]
        if ' and ' in Auth[-1]:
            last_auth = Auth[-1].split('and')
            items['authors'] = Auth[:-1] + last_auth
        else:
            items['authors'] = Auth

        Mant = [a for a in ps[3].contents[0].split(':')[1].split(',') ]
        if ' and ' in Mant[-1]:
            last_mant = Mant[-1].split('and')
            items['mantainers'] = Mant[:-1] + last_mant
        else:
            items['mantainers'] = Mant

    else:
        logging.warning(f"error with {items['name']} - parsing")
        logging.warning(f"could not pare part1 of {items['name']} description, authors and mantainers are set to None")
    return(items)


def existInstallation(bs, items):
    instr = [a for a in bs if a.name == 'pre']
    if len(instr)>0:
        items['Installation instructions'] = True
    else:
        items['Installation instructions'] = False

    return(items)


def parseDocumentation(bs, items):
    items['documentation'] = {}

    table = [a for a in bs if a.name == 'table']
    if len(table) > 1:
        logging.warning(f"error with {items['name']} - parsing")
        logging.warning(f"error in documentation parsing of {items['name']}. documentation is set to None")
        return(items)
        
    else:
        table = table[0]

        for row in table.findAll('tr'):
            cells = [cell for cell in row.findAll('td')]
            if cells[0].find('a') or cells[1].find('a'):
                doc = str(cells[0].find('a'))
                script = str(cells[1].find('a'))
                field = cells[2].get_text()
                field = field.replace('.','_')
                items['documentation'][field] = [doc, script]
        return(items)


def parseDetails(bs, items):
    table = [a for a in bs if a.name == 'table']
    if len(table) > 1:
        logging.warning(f"error with {items['name']} - parsing")
        logging.warning(f"error in details parsing of {items['name']}. Details set to None")
        return(items)

    else:
        table = table[0]
        for row in table.findAll('tr'):
            cells = [cell for cell in row.findAll('td')]
            if re.sub('\s+', '', cells[1].get_text() ) in ['', ' ']:
                items[cells[0].get_text()] = None
            else:
                items[cells[0].get_text()] = re.sub('\s+', '', cells[1].get_text() )

        return(items)

def parseArchives(bs, items):
    table = [a for a in bs if a.name == 'table']
    if len(table) > 1:
        logging.warning(f"error with {items['name']} - parsing")
        logging.warning(f"Error in archives parsing of {items['name']}. Archives set to None")
        return(items)
        
    else:
        table = table[0]

        for row in table.findAll('tr'):
            cells = [cell for cell in row.findAll('td')]
            if re.sub('\s+', '', cells[1].get_text() ) in ['', ' ']:
                items[cells[0].get_text()] = None
            else:
                items[cells[0].get_text()] = re.sub('\s+', '', cells[1].get_text() )

        return(items)


def parseInput(InputPath):
    '''
    This function takes the input file and builds a list of urls
    required: the input path. Line format in input file: <url>\t<name>\n
    returns: a lis of urls in the file
    '''
    def check_protocol(url):
        '''
        This function checks if the url has the protocol specified 
        and adds it if not
        '''
        if re.match('^http', url) != None: # To be changed for proper regex (beginning of line)
            return(url)
        else:
            return('https://' + url)

    try:
        urls_file = open(InputPath, 'r')
    except IOError:
        logging.error('error - crucial_variable_not_set')
        logging.error(f"Input file ({InputPath}) not found. Exiting...")
        logging.info("end_importation")
        exit(1)
    else:
        urls = []
        counter = 1
        counter_valid = 0
        for line in urls_file:
            if len(line.split('\t')) != 2:
                logging.warning(f"Input file line {counter} skipped: impossible to parse, number of columns != 2.")
            else:
                url = line.split('\t')[0]
                match = re.search("^(https?:\/\/)?(www.)?bioconductor.org\/packages\/[A-Za-z0-9_.]{1,15}\/bioc\/html\/.*?$", url)
                if match:
                    urls.append(check_protocol(url))
                    counter_valid += 1
            counter += 1
        logging.info(f"Number of URLs to be analyzed: {counter_valid}")

        if len(urls) == 0:
            logging.error("No URLs to be analyzed. Provided file ({InputPath}) seems to be empty. Exiting...")
            exit(1)

        else:
            return(urls)


def parse_links(items_urls, url):
    if items_urls != None:
        links = [url] + [items_urls]
    else:
        links = [url]
    
    return(links)

def clean_fields(items):
    # Build lists
    list_fields = ['Depends on me', 'Suggests', 'Suggests Me', 'Depends']
    for field in list_fields:
        if field in items and items[field]:
            items[field] = items[field].split(',')
    # Remove dots
    keys=tuple(items.keys())
    for key in keys:
        if '.' in key:
            items[key.replace('.', '_')] = items[key]
            items.pop(key, None)
    return(items)

def retrieve_metadata(bs, url, driver):
    '''
    Main function to retrieve metadata from a Bioconductor package page.
    bs: the beautiful soup object
    url: the url of the package page
    driver: the selenium driver
    '''
    items = {} # dictionary to store all the metadata
    name, citation, part1, part2 = preParsing(bs) # pre-parsing
    items['name'] = name
    items['citation'] = citation
    

    # Parsing first part of the page
    items = parsePart1(part1, items)

    # Parsing second part of the page
    instRaw, docRaw, detailsRaw, packRaw = getInsDocDetArch(part2) 
    items = existInstallation(instRaw, items)
    items = parseDocumentation(docRaw, items)
    items = parseDetails(detailsRaw, items)
    items = parseArchives(packRaw, items)

    items['links'] = parse_links(items['URL'],url)

    # Extracting publication
    items['publication'] = get_publication(url, driver)

    items = clean_fields(items)

    # Add provenance metadata
    items['@date'] = datetime.datetime.now().isoformat()
    items['@data_source'] = 'bioconductor'
    items['@source_url'] = url
    items['@id'] = f"https://openebench.bsc.es/monitor/tool/bioconductor:{name}:{items['Version']}/lib"

    return(items)


def import_data():
    # 1. getting arguments, log level and env variables of program

    ## 1.1. getting arguments
    arguments_parser = program_arguments_parser()
    arguments = get_program_arguments(arguments_parser)

    ## 1.2. setting log level
    numeric_level = getattr(logging, arguments.loglevel.upper())
    logs_dir = arguments.logdir
    logging.basicConfig(level=numeric_level, format='%(asctime)s - %(levelname)s - bioconductor - %(message)s', filename=f'{logs_dir}', filemode='w')
    
    ## 1.3. getting env variables
    load_dotenv(arguments.env_file)

    REMOTE = os.getenv('REMOTE')

    if REMOTE:
        logging.debug(f'Remote is set to {REMOTE}')
    else:
        logging.error('error - crucial_variable_not_set')
        logging.error("REMOTE is not set. Exiting...")
        logging.info("end_importation")
        exit(1)
        
    HOST = os.getenv('HOST', 'localhost') # this is the host of the selenium driver
    
    # 2. connect to DB/ get output file
    STORAGE_MODE = os.getenv('STORAGE_MODE', 'db')

    # Some logging
    logging.info("start_importation")

    if STORAGE_MODE =='db':
        alambique = connect_db()
        
    else:
        OUTPUT_PATH = os.getenv('OUTPUT_PATH', './')
    
    # 3. initialize driver
    driver = Driver(host=HOST, remote=REMOTE)
    
    # 4. Fetch packages
    ## if enricher == True, then we use the input file
    if arguments.enricher == True:
        urls_file = os.getenv('PACKAGES_URLS_PATH', './tmp/bioconductors_urls.txt')
        urls = parseInput(urls_file)

    ## else (if .enricher == False), then we fetch all packages from bioconductor
    else:
        urls = fetch_all_packages()

    
    if urls:
        logging.info("Requesting packages URLs...")
        
        for url in urls:

            # 5. Get bsObject from URL
            bsO = getHTML(url)
            
            if bsO:
                # 6. Retrieve metadata
                logging.debug(f'Retrieving metadata from object obtained from URL {url}')
                instance_dict = retrieve_metadata(bsO, url, driver.driver)
                
                # 7. Save data in DB or file
                if STORAGE_MODE=='db':
                    logging.debug(f'Pushing {instance_dict["name"]} to database')
                    push_entry(instance_dict, alambique)
                else:
                    save_entry(instance_dict, OUTPUT_PATH)
            else:
                logging.warning(f"error with {url} - parsing")
                logging.warning(f'could not extract bsObject from URL {url}')

    else:
        logging.error('error - crucial_object_empty')
        logging.error('No URLs found. Exiting...')
        logging.info("end_importation")
        exit(1)
    
    logging.info("end_importation")
        


if __name__ == '__main__':
    # This will become the main function
    import_data()
