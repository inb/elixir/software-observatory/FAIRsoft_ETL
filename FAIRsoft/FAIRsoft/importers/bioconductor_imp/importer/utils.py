import json 
import os
import requests
import logging
from bs4 import BeautifulSoup

# initializing session
session = requests.Session()
headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit 537.36 (KHTML, like Gecko) Chrome",
"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}

# the html retriever
def getHTML(url):
    '''
    Takes and url as an input and returns the corresponding
    bs4 object
    '''
    from bs4.dammit import EncodingDetector
    try:
        re = session.get(url, headers=headers, timeout=(10, 30))
    except:
        logging.warning(f'error with {url} - html_request')
        return(None)
    else:
        if re.status_code == 200:
            # dealing with encoding
            http_encoding = re.encoding if 'charset' in re.headers.get('content-type', '').lower() else None
            html_encoding = EncodingDetector.find_declared_encoding(re.content, is_html=True)
            encoding = html_encoding or http_encoding

            # generating BeautifulSoup object
            bsObj = BeautifulSoup(re.content, 'html5lib', from_encoding=encoding)

            return(bsObj)
        else:
            return(None)


def push_entry(tool:dict, collection:'pymongo.collection.Collection'):
    '''Push tool to collection.

    tool: dictionary. Must have at least an '@id' key.
    collection: collection where the tool will be pushed.
    log : {'errors':[], 'n_ok':0, 'n_err':0, 'n_total':len(insts)}
    '''
    # 4. Push to collection
    # date objects cause trouble and are prescindable
    if 'about' in tool.keys():
            tool['about'].pop('date', None)
    try:
        updateResult = collection.update_many({'@id':tool['@id']}, { '$set': tool }, upsert=True)
    except Exception as e:
        logging.warning(f"error with {tool['@id']} - pushing_to_db")
        logging.warning(e)
    else:
        logging.info(f"pushed_to_db_ok - {tool['@id']}")
    finally:
        return


def save_entry(tool, output_file):
    '''Save tool to file.

    tool: dictionary. Must have at least an '@id' key.
    output_file: file where the tool will be saved.
    log : {'errors':[], 'n_ok':0, 'n_err':0, 'n_total':len(insts)}
    '''
    # 4. Push to file
    # date objects cause trouble and are prescindable

    if 'about' in tool.keys():
            tool['about'].pop('date', None)
    try:
        if os.path.isfile(output_file) is False:
            with open(output_file, 'w') as f:
                json.dump([tool], f)
        else:
            with open(output_file, 'r+') as outfile:
                data = json.load(outfile)
                data.append(tool)
                # Sets file's current position at offset.
                outfile.seek(0)
                json.dump(data, outfile)

    except Exception as e:
        logging.warning(f"error with {tool['@id']} - pushing_to_db")
        logging.warning(e)

    else:
        logging.info(f"pushed_to_db_ok - {tool['@id']}")
    finally:
        return

def connect_db():
    '''Connect to MongoDB and return the database and collection objects.

    '''
    from pymongo import MongoClient
    ALAMBIQUE = os.getenv('ALAMBIQUE', 'alambique')
    HOST = os.getenv('DBHOST', 'localhost')
    PORT = os.getenv('DBPORT', 27017)
    DB = os.getenv('DB', 'observatory')
    
    client = MongoClient(HOST, int(PORT))
    alambique = client[DB][ALAMBIQUE]

    return alambique