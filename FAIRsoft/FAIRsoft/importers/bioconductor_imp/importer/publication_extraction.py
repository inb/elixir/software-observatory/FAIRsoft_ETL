import subprocess
import json
import logging

from selenium.webdriver.common.by import By

def extract_bioc_citation(driver):
    '''
    This function extracts the citation from the bioc page
    driver: the driver with the bioc page loaded 
    '''
    try:
        # extract citation 
        citation = driver.find_element(By.CLASS_NAME, 'bioc_citation')
        return citation

    except:
        return None

def extract_citation_link(citation):
    try:
        # extract link
        link = citation.find_element(By.TAG_NAME,'a')
        link = link.get_attribute('href')
        return link
    except:
        return None

def parse_citation(citation):
    '''
    Parse the citation extracting from web using anystyle software
    citation: string, the citation to be parsed
    '''
    def save_citation(citation, filename):
        with open(filename, 'w') as file_name:
            file_name.write(citation)

    try:
        filename = "tmp_citation.txt"
        save_citation(citation, filename) # Save the citation in a file to be parsed. Needed because anystyle does not accept input from stdin
        
        # Execute anystyle
        command = ["anystyle","--stdout","-f", "json","parse",filename]
        process = subprocess.run(args = command, stdout=subprocess.PIPE).stdout
        parsed = json.loads(process.decode("utf-8"))[0]

    except Exception as e:
        logging.warning(f"Error with NA - parsing")
        logging.warning(f"Error in citation parsing: {citation}. Returning the citation as is")
        logging.warning(e)

        parsed = citation
    
    finally:
        return(parsed)


def get_publication(url, driver):
    driver.get(url)
    
    citation = extract_bioc_citation(driver)
    link = extract_citation_link(citation)

    if citation:
        try:
            pub_cit = parse_citation(citation.text)
        except Exception as e:
            logging.warning(f"Error with NA - parsing")
            logging.warning(f"Error in citation parsing: {citation}. Returning the citation as Empty")
            logging.warning(e)

            pub = {}
        else:
            pub = {"url":link, "citation": pub_cit}
        
    else:
        pub = {}

    return(pub)