import unittest
from dotenv import load_dotenv

from importer.publication_extraction import get_publication
from importer.main import Driver, retrieve_metadata
from importer.utils import getHTML


class test_driver(unittest.TestCase):
    def test_selenium_remote_initialization(self):
        '''
        Tests that the driver is initialized correctly when Selenium grid and 
        firefox are running in docker containers
        '''
        driver = Driver(host='localhost', remote=True)
        # get google and get title
        driver.driver.get("http://www.google.com")
        self.assertEqual('Google', driver.driver.title)
        driver.driver.quit()
    
class test_publication_extraction(unittest.TestCase):

    def test_get_publication_local_positive(self):
        '''
        Tests that the function returns a citation object different from an 
        empty dictionary
        '''
        # This will become a test
        driver = Driver(host='localhost', remote=True)
        url = 'https://bioconductor.org/packages/release/bioc/html/BioNAR.html'
        citation = get_publication(url, driver.driver)
        driver.driver.quit()
        
        # Citation retrived at all
        self.assertNotEqual({},citation)


    def test_get_publication_local_positive_fields(self):
        '''
        Tests that the function returns a citation object with the correct fields
        '''
        # This will become a test
        driver = Driver(host='localhost', remote=True)
        url = 'https://bioconductor.org/packages/release/bioc/html/BioNAR.html'
        citation = get_publication(url, driver.driver)
        driver.driver.quit()

        # Citation has the correct field
        self.assertEqual(['BioNAR: Biological Network Analysis in R'], citation['citation']['title'])
        
    
class test_metadata_extraction(unittest.TestCase):


    def test_metadata_extraction_from_url(self):
        driver = Driver(host='localhost', remote=True)
        url = 'https://bioconductor.org/packages/release/bioc/html/BioNAR.html'
 
        bsO = getHTML(url, verb=False)
        instance_dict = retrieve_metadata(bsO, url, driver.driver)
        self.assertEqual('BioNAR', instance_dict['name'])


