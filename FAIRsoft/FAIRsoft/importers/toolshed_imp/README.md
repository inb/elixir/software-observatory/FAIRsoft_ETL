# Galaxy Toolshed importer 


This program extracts metadata from  
- (i) Galaxy Toolshed repositories metadata (see *repos_metadata_importer.py* and *galaxy_metadata.py*) 
- (ii) Galaxy Toolshed recipes (see *repos_config_importer.py) 

and store it either in a JSON file or pushed to a MongoDB database. 

The two extractions (i and ii) are run sequentially. In order to access the recipes (ii), which are XML configuration files, the repositories need to be downloaded and decompressed first. The download URLs are built from the repositories metadata (i) , which is why the two extractions are run sequentially, being the second dependent on the first.

## Usage

```sh
FAIRsoft_import_toolshed -l=[log-level] -d=[log-directory]
``` 

- `-l`/`--loglevel` is optional. It can be `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`. Default is `INFO`.
- `--logs-dir`/`-d` is optional. It specifies the path to the directory where the logs will be stored. Default is `./logs/`.

## Configuration

### Environment variables 

| Name             | Description | Default | Notes |
|------------------|-------------|---------|-------|
| STORAGE_MODE     |  Specifies whether the output will be stored in filesystem (`filesystem`) or pushed to a database (`db`) |  `db` |            |
| DBHOST       |  Host of database where output will be pushed |   `localhost`        |  Only used when STORAGE_MODE is `db`      |
| DBPORT       |  Port of database where output will be pushed |   `27017`            |  Only used when STORAGE_MODE is `db`      |
| DB         |  Name of database where output will be pushed |   `observatory`      |  Only used when STORAGE_MODE is `db`      |
| ALAMBIQUE |  Name of database where output will be pushed  |   `alambique`        |  Only used when STORAGE_MODE is `db`      |
| OUTPUT_PATH      |  Path to output file                    | `./data/bioconda.json` |  Only used when STORAGE_MODE is `filesystem` | 
| GALAXY_METADATA | Path to metadata extracted from Galaxy Metadata. This JSON file, automatically generated after the extraction of repositories metadata, constains identifiers that are necessary for the download of repositories, which contain the recipes.  | `./data/galaxy_metadata.json` | |

