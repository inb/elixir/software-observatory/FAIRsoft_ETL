# Repositories importer - GitHub and Bitbucket

The OpenEBench tools database contains software metadata gathered from different sources, including links to source code repositories. The code in this repository uses those links as a starting point to extract additional software metadata, one metadata record per software.  After the extraction is completed, `@data_source` and `@id` attributes are added to each record, which is then either stored in a JSON file or pushed to a MongoDB database.

Currently, this program extracts metadata from GitHub and Bitbucket repositories using the [*repoEnricher*](https://github.com/inab/opeb-enrichers/tree/master/repoEnricher) tool. 


## Usage

```sh
FAIRsoft_import_repositories
``` 

## Configuration

### Environment variables 
They can be set in an `.env` file:

| Name             | Description | Default | Notes |
|------------------|-------------|---------|-------|
| STORAGE_MODE     |  Specifies whether the output will be stored in filesystem (`filesystem`) or pushed to a database (`db`) |  `db` |            |
| DBHOST       |  Host of database where output will be pushed |   `localhost`        |  Only required when STORAGE_MODE is `db`      |
| DBPORT       |  Port of database where output will be pushed |   `27017`            |  Only required when STORAGE_MODE is `db`      |
| DB         |  Name of database where output will be pushed |   `observatory`      |  Only required when STORAGE_MODE is `db`      |
| ALAMBIQUE |  Name of database where output will be pushed |   `alambique`        |  Only required when STORAGE_MODE is `db`      |
| OUTPUT_PATH      |  Path to output file                          | `./data/repositories.json` |  Only required when STORAGE_MODE is `filesystem` | 
| REPOENRICHER_PATH | Path to [*repoEnricher*](https://github.com/inab/opeb-enrichers/repoEnricher) program     | `./opeb-enrichers/repoEnricher/repoEnricher.pl`      | |
| REPOENRICHER_OUTPUT_PATH | Path to *repoEnricher* output files | `./data/output` | |


### *repoEnricher* configuration file
The [*repoEnricher*](https://github.com/inab/opeb-enrichers/tree/master/repoEnricher) credentials to access GitHub and BitBucket APIs. Details [here](https://github.com/inab/opeb-enrichers/tree/master/repoEnricher/README.md) 




