# OPEB Tools importer 

Imports those entries in OPEB that were originally retrieved from bioconda and biotools.

## Set-up and Usage

```sh
FAIRsoft_import_opeb_tools -l=[log-level] -d=[log-directory]
``` 
`-l/--loglevel` argument can be `debug`, `info`, `warning`, `error` or `critical`. It is optional and defaults to `info`.
`-d/--logdir` argument is optional and defaults to `./logs/`.


## Configuration

### Environment variables 

| Name             | Description | Default | Notes |
|------------------|-------------|---------|-------|
| DBHOST       |  Host of database where output will be pushed |   `localhost`        |  Only used when STORAGE_MODE is `db`      |
| DBPORT       |  Port of database where output will be pushed |   `27017`            |  Only used when STORAGE_MODE is `db`      |
| DB         |  Name of database where output will be pushed |   `observatory`      |  Only used when STORAGE_MODE is `db`      |
| ALAMBIQUE |  Name of database where output will be pushed  |   `alambique`        |  Only used when STORAGE_MODE is `db`      |
| OUTPUT_PATH      |  Path to output file                    | `./data/bioconda.json` |  Only used when STORAGE_MODE is `filesystem` | 
| URL_OPEB_TOOLS | URL to OpenEBench Tools API | `https://openebench.bsc.es/monitor/tool` | |

