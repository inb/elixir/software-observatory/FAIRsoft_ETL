import configparser
from pymongo import MongoClient

def connect_db(configfile):    
    config = configparser.ConfigParser()
    config.read(configfile)
    DBHOST = config['MONGO_DETAILS']['DBHOST']
    DBPORT = config['MONGO_DETAILS']['DBPORT']
    DATABASE = config['MONGO_DETAILS']['DATABASE']
    ALAMBIQUE = config['MONGO_DETAILS']['ALAMBIQUE']

    connection = MongoClient(DBHOST, int(DBPORT))
    alambique = connection[DATABASE][ALAMBIQUE]
    print('Connected to database successfully')
    return(alambique)

def delete_by_source(sources, configfile):
    '''
    This function removes all entries in alambique from the sources specified 
    in `sources` sources is a list of sources names ('biotools', 'metrics',
    'bioconductor', 'galaxy', 'toolshed', 'bioconda_recipes','bioconda_conda','repositories', 'bioconda',
    'sourceforge', galaxy_metadatas')
    '''
    alambique = connect_db(configfile)
    for source in sources:
        alambique.remove({"@data_source":source})




